# Loco Gettext template
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Front-end submit for Coupon WP\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: Mon Jul 11 2016 22:00:36 GMT+0700 (ICT)\n"
"POT-Revision-Date: Mon Jul 11 2016 22:00:37 GMT+0700 (ICT)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/"

#: ../coupon-submit.php:98
msgid "Please enter coupon title."
msgstr ""

#: ../coupon-submit.php:99
msgid "Please enter coupon description."
msgstr ""

#: ../coupon-submit.php:100
msgid "Please select a store."
msgstr ""

#: ../coupon-submit.php:101
msgid "Please select a category."
msgstr ""

#: ../coupon-submit.php:102
msgid "Store does not exists please select other."
msgstr ""

#: ../coupon-submit.php:103
msgid "Please select coupon type."
msgstr ""

#: ../coupon-submit.php:104
msgid "Please enter image url."
msgstr ""

#: ../coupon-submit.php:105
msgid "Please select a image to upload"
msgstr ""

#: ../coupon-submit.php:106
msgid "Please agree to the Terms and Conditions before submit."
msgstr ""

#: ../coupon-submit.php:107
msgid ""
"Please enter coupon expires. If you don't know expires let check Don't know "
"the expiration date."
msgstr ""

#: ../coupon-submit.php:108
msgid "Please enter coupon code."
msgstr ""

#: ../coupon-submit.php:109
msgid "You have not permission to edit this coupon"
msgstr ""

#: ../coupon-submit.php:110
msgid "An error occurred please try again later"
msgstr ""

#: ../coupon-submit.php:632
msgid "Your coupon has been saved."
msgstr ""

#: ../coupon-submit.php:638
msgid "Your coupon published"
msgstr ""

#: ../coupon-submit.php:640
msgid "Your coupon is pending review "
msgstr ""

#: ../coupon-submit.php:660
msgid "There was some errors with your submission"
msgstr ""

#: ../coupon-submit.php:689
msgid "Oops! missing plugin"
msgstr ""

#: ../coupon-submit.php:691
msgid "You must activate wpcoupons plugin to use this function."
msgstr ""

#: ../coupon-submit.php:768
msgid "Select Store *"
msgstr ""

#: ../coupon-submit.php:807
msgid "Select Category *"
msgstr ""

#: ../coupon-submit.php:827
msgid "Offer Type *"
msgstr ""

#: ../coupon-submit.php:828
msgid "Coupon Code"
msgstr ""

#: ../coupon-submit.php:829
msgid "Sale"
msgstr ""

#: ../coupon-submit.php:830
msgid "Printable"
msgstr ""

#: ../coupon-submit.php:835
msgid "Add code or change offer type"
msgstr ""

#: ../coupon-submit.php:836
msgid "Code"
msgstr ""

#: ../coupon-submit.php:842
msgid "Coupon image"
msgstr ""

#: ../coupon-submit.php:845
msgid "Print coupon image"
msgstr ""

#: ../coupon-submit.php:847
msgid "Click icon to switch input method"
msgstr ""

#: ../coupon-submit.php:850
msgid "Image URL"
msgstr ""

#: ../coupon-submit.php:852
msgid "Upload a image"
msgstr ""

#: ../coupon-submit.php:853
msgid "Input image URL"
msgstr ""

#: ../coupon-submit.php:879
msgid "Coupon Aff URL"
msgstr ""

#: ../coupon-submit.php:885
msgid "Exp Date : dd/mm/yyyy"
msgstr ""

#: ../coupon-submit.php:894
msgid "Don't know the expiration date."
msgstr ""

#: ../coupon-submit.php:898
msgid "Offer Title"
msgstr ""

#: ../coupon-submit.php:902
msgid "Offer Description"
msgstr ""

#: ../coupon-submit.php:912
msgid "Submit"
msgstr ""

#. Name of the plugin
msgid "Front-end submit for Coupon WP"
msgstr ""

#. URI of the plugin
msgid "http://famethemes.com/plugins/wp-coupon-submit"
msgstr ""

#. Description of the plugin
msgid "Submit coupon on front-end."
msgstr ""

#. Author of the plugin
msgid "famethemes"
msgstr ""

#. Author URI of the plugin
msgid "http://famethemes.com"
msgstr ""

#. Base ID
#: ../widget.php:15
msgid "Coupon Submit"
msgstr ""

#: ../widget.php:16
msgid "Display submit coupon form."
msgstr ""

#: ../widget.php:59
msgid "Submit new coupon"
msgstr ""

#: ../widget.php:60
msgid "Submit {store_name}'s Coupon"
msgstr ""

#: ../widget.php:63
msgid "Title:"
msgstr ""

#: ../widget.php:68
msgid "Single Store Title:"
msgstr ""

#: ../widget.php:71
msgid ""
"The special title that will display on store page instead of Title, "
"available tag: {store_name}"
msgstr ""
