

jQuery( document ).ready( function( $ ) {
    $( '.st-datepicker').each( function () {
        var f = $( this );
        var format = f.data('format') || 'dd/mm/yy',
            alt = f.data('alt');

        f.datepicker( {
            dateFormat: format,
            altField: alt,
            altFormat: "yy-mm-dd"
        } );

    } );
    // switch input printable image/upload
    $( '.c-input-switcher').each( function() {
        var p = $( this );
        p.on( 'click', '.for-input',function ( e ) {
            e.preventDefault();
            $( this).hide();
            $( '.file-input', p ).hide();
            $( '.for-upload, .text-input', p ).show();
            $('input[name=coupon_image_type]', p ).val('url');
        } );
        p.on( 'click', '.for-upload' ,function ( e ) {
            e.preventDefault();
            $( this).hide();
            $( '.text-input, .for-input', p ).hide();
            $( '.for-input, .file-input', p ).show();
            $('input[name=coupon_image_type]', p ).val('upload');
        } );
    } );

    //

    $( 'form .field-coupon-type select').change( function ( ) {
        var  s = $( this );
        var p = s.parents('form');
        var v = s.val();
        //alert( v );
        $( '.field-code, .c-tile-print' ,p).hide();
        $('.c-tile-others', p ).show();
        if ( v == 'sale' ) {
            $( '.field-print, .field-code' ,p).hide();
        } else if( v == 'print' ) {
            $( '.field-code, .c-tile-others' ,p).hide();
            $( '.c-tile-print' ,p).show();
        }  else {
            $( '.field-code' ,p).show();
        }
    } );

    $( 'form .field-coupon-type select').trigger('change');

    // validate coupon form
    $( '.st-coupon-form' ).submit( function () {
        var f = $( this );
        f.addClass('loading');
        f.removeClass( 'success error' );
        var values = f.form('get values');
        var is_error = false;

        if ( typeof values.coupon_store !== undefined ) {
            if ( values.coupon_store  == '' || values.coupon_store  == 0 ) {
                $( 'select[name=coupon_store]', f).closest('.field').addClass('error');
                is_error = true;
            } else {
                $( 'select[name=coupon_store]', f).closest('.field').removeClass('error');
            }
        }

        if ( typeof values.coupon_cat !== undefined ) {
            if ( values.coupon_cat  == '' || values.coupon_cat <= 0 ) {
                $( 'select[name=coupon_cat]', f).closest('.field').addClass('error');
                is_error = true;
            } else {
                $( 'select[name=coupon_cat]', f).closest('.field').removeClass('error');
            }
        }

        if ( typeof values.coupon_type !== undefined ) {
            // If is new the check image
            if ( typeof values.coupon_id === undefined || values.coupon_id == ''  ) {
                //alert( values.coupon_type.length );
                if (values.coupon_type == '') {
                    $('select[name=coupon_type]', f).closest('.field').addClass('error');
                    is_error = true;
                } else {
                    $('select[name=coupon_type]', f).closest('.field').removeClass('error');
                }

                if (values.coupon_type == 'print') {
                    // if is unput url
                    if (values.coupon_image_type == 'url') {
                        // check url if fill
                        if (values.coupon_image_url == '') {
                            $('input[name=coupon_image_url]', f).closest('.field').addClass('error');
                            is_error = true;
                        } else {
                            $('input[name=coupon_image_url]', f).closest('.field').removeClass('error');
                        }

                    } else if (values.coupon_image_file == '') { // check if select a file o not
                        $('input[name=coupon_image_file]', f).closest('.field').addClass('error');
                        is_error = true;
                    } else {
                        $('input[name=coupon_image_file]', f).closest('.field').removeClass('error');
                    }

                } else {
                    $('input[name=coupon_image_url]', f).closest('.field').removeClass('error');
                    $('input[name=coupon_image_file]', f).closest('.field').removeClass('error');
                }
            }
        }

        if ( typeof values.coupon_expires !== undefined ) {
            if ( values.coupon_expires == '' ) {
                if ( typeof values.coupon_expires_unknown !== undefined && values.coupon_expires_unknown == true ) {
                    $( 'input[name=coupon_expires]', f ).closest('.field').removeClass('error');
                } else {
                    $( 'input[name=coupon_expires]', f ).closest('.field').addClass('error');
                    is_error = true;
                }

            }
        }

        if ( typeof values.coupon_title !== undefined ) {
            if ( values.coupon_title == '' ) {
                $( 'input[name=coupon_title]', f ).closest('.field').addClass('error');
                is_error = true;
            } else {
                $( 'input[name=coupon_title]', f ).closest('.field').removeClass('error');
            }
        }

        if ( typeof values.coupon_title !== undefined ) {
            if ( values.coupon_title == '' ) {
                $( 'input[name=coupon_title]', f ).closest('.field').addClass('error');
                is_error = true;
            } else {
                $( 'input[name=coupon_title]', f ).closest('.field').removeClass('error');
            }
        }

        if ( typeof values.coupon_description !== undefined ) {
            if ( values.coupon_description == '' ) {
                $( 'textarea[name=coupon_description]', f ).closest('.field').addClass('error');
                is_error = true;
            } else {
                $( 'textarea[name=coupon_description]', f ).closest('.field').removeClass('error');
            }
        }

        if ( is_error ) {
            f.removeClass('loading');
            return false;
        }

        var processData = true, contentType = false;

        /**
         * Check if browsers support FormData object
         *
         * @see https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
         */
        var form_data = values;
        if ( typeof FormData !== "undefined" ) {
            form_data = new FormData( this );
            processData = false;
            contentType = false;
        } else {
            return true;
        }

        $.ajax( {
            url: ST.ajax_url,
            type: 'post',
            data: form_data,
            dataType: 'json',
            processData:  processData,
            contentType:  contentType,
            success: function( r ){
                f.removeClass('loading');
                if ( $( '.st-response-msg' ,f ).length > 0 ) {
                    $( '.st-response-msg' ,f).remove();
                }

                $( "input[name=coupon_image_file], input[name=coupon_image_url]", f).val('');

                if ( r.success ) {
                    f.addClass( 'success' );
                    // clear data if is add new
                    if (  typeof values.coupon_id == "undefined" || values.coupon_id == '' ) {
                        $('.st-datepicker').val('');
                        f.form('set values', {
                            coupon_image_url        : '',
                            coupon_aff_url          : '',
                            coupon_expires          : '',
                            coupon_expires_unknown  : false,
                            coupon_title            : '',
                            coupon_description      : ''
                        });
                    } else {
                        //$( 'input[name=coupon_id]' ).val( r.coupon_id );
                        if ( typeof r.image_url !== "undefined" ) {
                            if ( $( '.image-thumb', f ).length ) {
                                $( '.image-thumb', f).remove();
                            }
                            $( '.field-image', f).append('<p class="image-thumb"><img src="'+r.image_url+'" alt=""></p>');
                        }
                    }

                } else {
                    f.addClass( 'error' );
                }
                $( r.data ).insertBefore( $( ".btn_primary" , f ) );

                $( '.message .close', f ).click( function() {
                    $(this).closest('.message').transition('fade');
                });

                // auto close message
                setTimeout( function(){
                    $( '.st-response-msg', f ).remove();
                }, 5000 );

            }
        } );
        return false;

    } );


} ) ;