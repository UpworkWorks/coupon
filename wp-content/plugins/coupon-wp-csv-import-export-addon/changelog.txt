## 1.0.2
- Improve progress log.
- Update store, category data if exists.

## 1.0.0
- Release.