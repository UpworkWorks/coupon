<?php
/**
 * WP Coupon functions and definitions
 *
 * @package WP Coupon
 */
require $_SERVER['DOCUMENT_ROOT'].'/aws/aws-autoloader.php';
use Aws\S3\StreamWrapper;
use Aws\S3\S3Client;
/**
 * Define theme constants
 */
$theme_data   = wp_get_theme();
if ( $theme_data->exists() ) {
	define( 'ST_THEME_NAME', $theme_data->get( 'Name' ) );
	define( 'ST_THEME_VERSION', $theme_data->get( 'Version' ) );
}

/**
 * Check if WooCommerce is active
 * @return bool
 */
function wpcoupon_is_wc(){
	return class_exists( 'WooCommerce' );
}

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 950; /* pixels */
}

remove_filter('template_redirect', 'redirect_canonical');

if ( ! function_exists( 'wpcoupon_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wpcoupon_theme_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wp-coupon, use a find and replace
	 * to change 'wp-coupon' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wp-coupon', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Use shortcodes in text widgets.
	add_filter( 'widget_text', 'do_shortcode' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'wpcoupon_small_thumb', 200, 115, true );
	add_image_size( 'wpcoupon_medium-thumb', 480, 480, true );
	add_image_size( 'wpcoupon_blog_medium', 620, 300, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'wp-coupon' ),
		'footer' => esc_html__( 'Footer', 'wp-coupon' ),
	) );

	// This theme styles the visual editor to resemble the theme style.
	//add_editor_style( 'assets/css/editor-style.css' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

}
endif; // wpcoupon_theme_setup
add_action( 'after_setup_theme', 'wpcoupon_theme_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function wpcoupon_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'wp-coupon' ),
		'id'            => 'sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Page Sidebar', 'wp-coupon' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Coupon Category Sidebar', 'wp-coupon' ),
		'id'            => 'sidebar-coupon-category',
		'description'   => esc_html__( 'The sidebar will display on coupon category page.', 'wp-coupon' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

    register_sidebar( array(
        'name'          => esc_html__( 'Store Sidebar', 'wp-coupon' ),
        'id'            => 'sidebar-store',
        //'description'   => esc_html__( 'The sidebar will display on single store, coupon category.', 'wp-coupon' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

	register_sidebar( array(
		'name'          => esc_html__( 'WooCommerce Sidebar', 'wp-coupon' ),
		'id'            => 'sidebar-woo',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'wp-coupon' ),
		'id'            => 'footer-1',
		'description'   => wpcoupon_sidebar_desc( 'footer-1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'wp-coupon' ),
		'id'            => 'footer-2',
		'description'   => wpcoupon_sidebar_desc( 'footer-2' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'wp-coupon' ),
		'id'            => 'footer-3',
		'description'   => wpcoupon_sidebar_desc( 'footer-3' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'wp-coupon' ),
		'id'            => 'footer-4',
		'description'   => wpcoupon_sidebar_desc( 'footer-4' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Frontpage sidebar
	register_sidebar( array(
		'name'          => esc_html__( 'Frontpage Before Main Content', 'wp-coupon' ),
		'id'            => 'frontpage-before-main',
		'description'   => esc_html__( 'This sidebar display on frontpage template', 'wp-coupon' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Frontpage Main Content', 'wp-coupon' ),
		'id'            => 'frontpage-main',
		'description'   => esc_html__( 'This sidebar display on frontpage template', 'wp-coupon' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'has_unique'    => true
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Frontpage Main Sidebar', 'wp-coupon' ),
		'id'            => 'frontpage-sidebar',
		'description'   => esc_html__( 'This sidebar display on frontpage template', 'wp-coupon' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Frontpage After Main Content', 'wp-coupon' ),
		'id'            => 'frontpage-after-main',
		'description'   => esc_html__( 'This sidebar display on frontpage template', 'wp-coupon' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	 register_sidebar( array(
        'name'          => 'Newsletter in Modal',
        'id'            => 'newsletter-modal',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div> <!-- end .widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );

}
add_action( 'widgets_init', 'wpcoupon_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
 // Add async to javacript
add_filter( 'clean_url',function($url){if(!strpos($url,'.js')){return $url;}else{if(strpos($url,'jquery')){return $url;}else{return $url."' async='async";}}}, 11, 1 );
// function( $url ){if ( (FALSE === strpos( $url, '.js' )|| (TRUE === strpos($url,'jquery'))) ){return $url;}return $url."' async='async";}

function wpcoupon_theme_scripts() {

	// Stylesheet
	wp_enqueue_style( 'wpcoupon_semantic', get_template_directory_uri() .'/assets/css/semantic.min.css', array(), '4.2.0' );
	wp_enqueue_style( 'googlefont-latin', 'https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin', array(), '' );
	wp_enqueue_style( 'wpcoupon_style', get_template_directory_uri().'/style.css' );
	if ( is_rtl() ){
		wp_enqueue_style( 'wpcoupon_rtl', get_template_directory_uri() .'/rtl.css', array(), '4.2.0' );
	}

	// jQuery & Scripts
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'wpcoupon_libs', get_template_directory_uri() . '/assets/js/libs.js', array( 'jquery' ), '', false );
	wp_enqueue_script( 'wpcoupon_semantic', get_template_directory_uri() . '/assets/js/libs/semantic.min.js', array( 'jquery'  ), '', false );
	wp_enqueue_script( 'wpcoupon_global', get_template_directory_uri() . '/assets/js/global.js', '', false );
	
	
    $localize =  array(
        'ajax_url'        => admin_url( 'admin-ajax.php' ),
        'vote_expires'    => apply_filters( 'st_coupon_vote_expires', 7 ), // 7 days
        '_wpnonce'        => wp_create_nonce(),
        'user_logedin'    => is_user_logged_in(),
        'added_favorite'  => esc_html__( 'Favorited', 'wp-coupon' ),
        'add_favorite'    => esc_html__( 'Favorite This Store', 'wp-coupon' ),
        'login_warning'   => esc_html__( 'Please login to continue...', 'wp-coupon' ),
        'save_coupon'     => esc_html__( 'Save this coupon', 'wp-coupon' ),
        'saved_coupon'    => esc_html__( 'Coupon Saved', 'wp-coupon' ),
        'no_results'      => esc_html__( 'No results...', 'wp-coupon' ),
        'copied'          => esc_html__( 'Copied', 'wp-coupon' ),
        'copy'            => esc_html__( 'Copy', 'wp-coupon' ),
        'print_prev_tab'  => '0', // open store website in previous tab.
        'sale_prev_tab'   => '1', // open store website in previous tab.
        'code_prev_tab'   => '1', // open store website in previous tab.
    ) ;
    $list = '';
    if ( is_user_logged_in() ){
        $user = wp_get_current_user();
        $list  = get_user_meta( $user->ID, '_wpc_saved_coupons' , true );
        $stores = get_user_meta( $user->ID, '_wpc_favorite_stores' , true );
        $localize['my_saved_coupons'] =  explode( ',', $list );
        $localize['my_favorite_stores'] =  explode( ',', $stores );
    } else {
        $localize['my_saved_coupons'] = array();
        $localize['my_favorite_stores'] = array();
    }

	$localize['my_saved_coupons'] = explode( ',', $list );
    wp_localize_script( 'wpcoupon_global', 'ST', apply_filters( 'wp_coupon_localize_script', $localize ) );

}
add_action( 'wp_enqueue_scripts', 'wpcoupon_theme_scripts' );


/**
 * Helper lib
 */
require_once get_template_directory() . '/inc/core/helper.php';

/**
 * Theme Options
 */
if ( class_exists( 'ReduxFramework' ) ) {
    require_once( get_template_directory() . '/inc/config/option-config.php' );
}

/**
 * Theme one click to import demo content
 * Check if plugin https://wordpress.org/plugins/one-click-demo-import/
 *
 * @see https://wordpress.org/plugins/one-click-demo-import/faq/
 */
if ( class_exists( 'PT_One_Click_Demo_Import' ) ) {
    require_once( get_template_directory() . '/inc/config/demo-config.php' );
}

// Retrieve theme option values
if ( ! function_exists('wpcoupon_get_option') ) {
	function wpcoupon_get_option($id, $fallback = false, $key = false ) {
		global $st_option;
		if ( ! $st_option ) {
			$st_option = get_option('st_options');
		}
        if ( ! is_array( $st_option ) ) {
            return $fallback;
        }
		if ( $fallback == false ) $fallback = '';
		$output = ( isset($st_option[$id]) && $st_option[$id] !== '' ) ? $st_option[$id] : $fallback;
		if ( !empty($st_option[$id]) && $key ) {
			$output = $st_option[$id][$key];
		}
		return $output;
	}
}

/**
 * Recommend plugins via TGM activation class
 */
require_once get_template_directory() . '/inc/tgmpa/tgmpa-config.php';


/**
 * Post type
 */
require_once get_template_directory() . '/inc/post-type.php';


/**
 * Coupon functions.
 */
require_once get_template_directory() . '/inc/core/coupon.php';

/**
 * Coupon functions.
 */
require_once get_template_directory() . '/inc/core/store.php';

/**
 * Ajax handle
 */
require_once get_template_directory() . '/inc/core/ajax.php';

/**
 * Schedule event
 */
require_once get_template_directory() . '/inc/core/schedule-event.php';

/**
 * Auto update
 */
if ( is_admin() ) {
	require_once get_template_directory() . '/inc/core/admin-update.php';
}



/**
 * Load user functions
 */
require_once get_template_directory() . '/inc/user/user.php';


/**
 *Theme Hooks
 */
require_once get_template_directory() . '/inc/core/hooks.php';

/**
 * Custom template tags for this theme.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 * Custom CSS, JS, .. code
 */
require_once get_template_directory() . '/inc/custom-code.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require_once get_template_directory() . '/inc/extras.php';

/**
 * Load custom metaboxes config.
 */
require_once get_template_directory() . '/inc/config/metabox-config.php';

/**
 * The theme fully support WooCommerce, Awesome huh?.
 */
add_theme_support( 'woocommerce' );
require_once get_template_directory() . '/inc/config/woocommerce-config.php';


/**
 * Widgets
 */
require_once get_template_directory() . '/inc/widgets/_assets.php';
require_once get_template_directory() . '/inc/widgets/popular-stores.php';
require_once get_template_directory() . '/inc/widgets/categories.php';
require_once get_template_directory() . '/inc/widgets/newsletter.php';
require_once get_template_directory() . '/inc/widgets/carousel.php';
require_once get_template_directory() . '/inc/widgets/coupons.php';
require_once get_template_directory() . '/inc/widgets/sidebar.php';
require_once get_template_directory() . '/inc/widgets/headline.php';
require_once get_template_directory() . '/inc/widgets/slider.php';

if ( wpcoupon_is_wc() ) {
	/**
	 * WooCommerce Helpers
	 */
	require_once get_template_directory() . '/inc/woocomerce/woocomerce.php';
}

if ( defined( 'SITEORIGIN_PANELS_BASE_FILE' ) ) {
	/**
	 * Siteorigin Helpers
	 */
	require_once get_template_directory() . '/inc/siteorigin.php';
}

/* SHORTCODE */
function getPopshopResult( $atts ){
	$str = '<section id="coupon-listings-store" class="wpb_content_element"><div class="ajax-coupons"><div id="store-listings-wrapper" class="store-listings st-list-coupons">';
	$end = '</div></section>';
	$ps_catalog = "5zfsvv3taorq620p5bzxj3p92";
	$ps_account = "39lrl0itdd4mdwwbbxyp3ahbm";
	$startoffset = 1;
	$param = shortcode_atts( array(
        'per_page' => 20,
    ), $atts );
	$content = file_get_contents('https://www.popshops.com/v3/products.json?catalog='. $ps_catalog .'&account='. $ps_account.'&results_per_page='.$param['per_page'].'&page='.$startoffset);
	$content = json_decode($content);
	$products = $content->results->products->product;
	foreach ($products as $item) {
		$subdecs = substr($item->description,0,50);
		$str.= '<div data-id="'.$item->id.'" class="coupon-item has-thumb store-listing-item c-type-code coupon-listing-item shadow-box"><div class="store-thumb-link"><div class="store-thumb center-div">';
		$str.= '<a target="_blank" href="'.$item->offers->offer[0]->url.'">';
		$str.= '<img width="200" height="115" src="'.$item->image_url_large.'" class="attachment-wpcoupon_medium-thumb size-wpcoupon_medium-thumb" alt="athleteform">';
		$str.= '</a></div></div>';
		$str.='<div class="latest-coupon"><h3 class="coupon-title"><a class="coupon-link" rel="nofollow" title="'.$item->name.'" data-type="code" data-coupon-id="108" data-aff-url="'.$item->offers->offer[0]->url.'" target="_blank" href="'.$item->offers->offer[0]->url.'">'.$item->name.'</a></h3>';
        $str.='<div class="coupon-des"><div class="coupon-des-ellip">'.$subdecs.'<span class="c-actions-span">...<a class="more" href="#">More</a></span></div><div class="coupon-des-full"><p>'.$item->description.'<a class="more less" href="#">Less</a></p></div></div></div>';
		$str.='<div class="coupon-detail coupon-button-type"><a target="_blank" rel="nofollow" data-type="sale" data-coupon-id="105" data-aff-url="'.$item->offers->offer[0]->url.'" class="coupon-deal coupon-button" href="'.$item->offers->offer[0]->url.'">Get Deal <i class="shop icon"></i></a>';
        $str.='<div class="clear"></div></div>';
		$str.= '<div class="clear"></div>';
		$str.='</div>';
	}
	$str .= '</div><div class="wpb_content_element" style="text-align:center">';
	$str .='<a href="#" id="load-more-popshop" class="ui button btn btn_primary btn_large" data-next-page=""  data-loading-text="Loading...">Load More Coupons <i class="arrow circle outline down icon"></i></a>';
    $str .='</div>';
	$str.= $end;
	$str .= '<script>';
	$str .= 'var $ps_catalog = "'.$ps_catalog.'";';
	$str .= 'var $ps_account = "'.$ps_account.'";';
	$str .= 'var $startoffset = "'.++$startoffset.'";';
	$str .= 'var $perpage = "'.$param['per_page'].'";';
	$str .='</script>';
	$str .= '<script src="'.get_template_directory_uri().'/assets/js/customjs.js"></script>'; 
	return $str;
}
add_shortcode( 'popshopapi', 'getPopshopResult' );

function custom_get_term( $search, $tax, $term_meta = false ){
	$t = false;
	if ( term_exists( $search, $tax) ) {
		if ( is_int( $search ) ) {
			$t = get_term_by('id', $search, $tax );
		}
		if ( ! $t ) {
			$t = get_term_by('slug', sanitize_title( $search ), $tax);
		}
		if ( ! $t ) {
			$t = get_term_by( 'name', $search, $tax );
		}
		return $t;
	} elseif ($term_meta) {	// Try get store by site url
		global $wpdb;
		$result = $wpdb->get_results("SELECT term_id FROM `" .$wpdb->prefix. "termmeta` WHERE meta_key = '". $term_meta['key'] ."' AND meta_value = '". $term_meta['value'] ."'");
		if (! empty($result))
			return $result[0];
	}

	return false;
}

function processfmtc($atts) {
	global $wpdb;

	$params = shortcode_atts( array(
			'incremental' => 'true',
			'use_img_store_this_api' => false
	), $atts );
	
	$api_name = 'fmtc';
	$incremental = $params['incremental'] == 'true' ? '&incremental=1' : ''; 
	$fmtc_api_key = "9dc094744aec63e357ea526603712e64";
	
	$author = 1;
	$store_tax = 'coupon_store';
	$term_id = 0;
	$is_merchant_update = FALSE;
	
	// Get list categories from API
	$response_category_decode = get_response_from_api("http://services.fmtc.co/v2/getCategories?key=$fmtc_api_key&format=JSON&v2=1", TRUE);
	
	// Start check valid json
	$check_json = is_valid_json();
	if ($check_json !== TRUE) {
		return $check_json;
	}
	// End check valid json
	
	upsert_api_category($response_category_decode, $api_name);
	
	// Start get data from fmtc API
	$deals = get_response_from_api("http://services.fmtc.co/v2/getDeals?key=$fmtc_api_key&format=JSON&local=1&v2categories=1". $incremental);
	

	// Start check valid json
	$check_json = is_valid_json();
	if ($check_json !== TRUE) {
		return $check_json;
	}
	// End check valid json
	
	// End get data from fmtc API
	
	// Get merchants in DB
	$merchants_db = get_merchant_db('fmtc');
	
	// Get merchants in API
	$list_merchants_api = get_list_merchants_api($api_name, $deals, "http://services.fmtc.co/v2/getMerchants?key=$fmtc_api_key&format=JSON&merchantids=");
	
	// Get coupon api from DB
	$coupon_api_results = get_list_coupon_api($api_name, $deals);
	
	// Get coupon discount type in DB
	$coupon_discount = get_coupon_discount_db($api_name);
	
	// Get deal types from API
	$deal_types = get_deal_types_from_api('fmtc', "http://services.fmtc.co/v2/getTypes?key=$fmtc_api_key&format=JSON");
	
	foreach ($deals as $item) {
		$merchant_id = $item->nMerchantID;
		$term_meta_data_default = array();
		$id_store_image_post = 0;
		$link_store_image = 'http://www.offer.me/wp-content/themes/wp-coupon/assets/images/store_logo_default.jpg';	// We use default image if API not return
		$link_store_screenshot = '';
		
		// Get merchant info from API
		$merchant_from_api = $list_merchants_api[$merchant_id];
		$store_url = get_fmtc_url($merchant_from_api, 'store');

		// Get store logo url from API
		if (! empty($merchant_from_api->aLogos)) {
			$logo_url = get_fmtc_logo_url($merchant_from_api->aLogos);
			$link_store_image = $logo_url ? $logo_url : $link_store_image;
			$link_store_screenshot = get_fmtc_logo_url($merchant_from_api->aLogos, 'screenshot');
		}
		
		// START UPSERT STORE
		$t = custom_get_term($merchant_from_api->cName, 'coupon_store', array('key' => '_wpc_store_site_url', 'value' => rtrim($merchant_from_api->cHomepageURL, '/') ));
		$t_data = array(
				'name' => $merchant_from_api->cName,
				'slug' => sanitize_title($merchant_from_api->cName),
				'parent' => 0,
				'description' => 'Insert from API'
		);
		
		
		if ( $t ) {
			$term_id = $t->term_id;
			$term_meta_data = get_term_meta($term_id);
			foreach ($term_meta_data as $k => $v) {
				$term_meta_data_default[$k] = $v[0];
			}
			
			$merchant_filter = array(
					'api_name' => 'fmtc',
					'merchant_id' => $merchant_from_api->nMerchantID,
					'merchant_name' => $merchant_from_api->cName,
					'site_url' => rtrim($merchant_from_api->cHomepageURL, '/'),
					'url_logo' => $link_store_image,
					'api_url' => $store_url,
			);
			
			$is_merchant_change = check_merchant_change($merchant_from_api->nMerchantID, $merchants_db, $merchant_filter);
			
			if ($is_merchant_change) {
				wp_update_term( $term_id , $store_tax, $t_data ); // Only update store when merchant from api update
				// If merchant update, we update relative term_meta fields
				$term_meta_data_default['_wpc_store_url'] = $store_url;
				$term_meta_data_default['_wpc_store_name'] = $merchant_from_api->cName;
				$term_meta_data_default['_wpc_store_site_url'] = rtrim($merchant_from_api->cHomepageURL, '/');
			}
		} else {
			$t = wp_insert_term($merchant_from_api->cName, $store_tax, $t_data );
			if ( $t && ! is_wp_error( $t ) ) {
				$t = ( object ) $t;
				$term_id =  $t->term_id;
			} else {
				die("Insert term error");
			}
			
			$linkS3 = upload_to_s3($link_store_image);
			$store_image_post_data = array();
			$store_image_post_data['post_title'] =  $linkS3;
			$store_image_post_data['guid'] =  $linkS3;
			$id_store_image_post = wp_insert_post( $store_image_post_data );
			
			// Set store category name
			$store_category_name = 'Other';
			if (isset($merchant_from_api->cPrimaryCategory) && $merchant_from_api->cPrimaryCategory) {
				$store_category_name = get_store_category_name($api_name, $merchant_from_api->cPrimaryCategory, $merchant_from_api->aCategoriesV2);
			}
			$term_meta_data_default = array(
					'_wpc_store_url'       => $store_url,
					'_wpc_store_aff_url'   => '',
					'_wpc_store_name'      => $merchant_from_api->cName,
					'_wpc_store_heading'   => '',
					'_wpc_is_featured'     => 'on',
					'_wpc_count_posts'     => 0,
					'_wpc_extra_info'      => '',
					'_wpc_store_image_id'  => $id_store_image_post,
					'_wpc_store_image'     => $linkS3,
					'_wpc_store_source_image' => $link_store_image,
					'_wpc_store_site_url' => rtrim($merchant_from_api->cHomepageURL, '/'),
					'_wpc_store_category_name' => $store_category_name
			);
			add_post_meta($id_store_image_post, '_wp_attached_file', $linkS3, true);
		}
		
		if ( ! $term_id || is_wp_error( $term_id ) ) {
			return false;
		}
		
		if ( function_exists( 'update_term_meta' ) ) {
			foreach ( $term_meta_data_default as $k => $v ) {
				update_term_meta( $term_id, $k, $v );
			}
		}
		// END UPSERT STORE
		
		// START UPSERT COUPON
		$post_id = 0;
		$deal_content = get_deal_content($api_name, $item, $deal_types);
		$deal_url = get_fmtc_url($item, 'deal');
		
		if (isset($coupon_api_results[$item->nCouponID])) {
			$get_post = (array) get_post($coupon_api_results[$item->nCouponID]);
			// Check coupon is exist in DB 
			if (! empty($get_post)) {
				$post_id = $get_post['ID'];
				// Only update post(coupon) when updated
				if ($get_post['post_title'] != $item->cLabel || $get_post['post_content'] != $deal_content) {
					$get_post['post_title'] = $item->cLabel;
					$get_post['post_content'] = $deal_content;
					wp_update_post($get_post);
				}
				
				$post_meta_data = get_post_meta($post_id);
				$coupon_meta_filter = array(
						'_wpc_coupon_type' 		=> in_array('coupon', $item->aTypes) && $item->cCode ? 'code' : 'sale',
						'_wpc_expires'     		=> strtotime($item->dtEndDate),
						'_wpc_coupon_type_code' => in_array('coupon', $item->aTypes) && $item->cCode ? $item->cCode : '',
						'_wpc_destination_url'  => $deal_url,
						'_wpc_coupon_source_image' => $item->cImage ? $item->cImage : $link_store_screenshot,
						'_wpc_store'	=> array($term_id)
				);
				
				// Update coupon meta in DB if coupon data from API update.
				check_coupon_meta_change($post_id, $post_meta_data, $coupon_meta_filter);
			}
		}
		
		if (! $post_id) {
			$post_data = array(
					'post_title'    => $item->cLabel,
					'post_content'  => $deal_content,
					'post_excerpt'  => '',
					'post_date'     => date_i18n('Y-m-d H:i:s'),
					'post_author'   => $author,
					'post_type'     => 'coupon',
					'post_status'   => 'publish',
			);
			$post_id = wp_insert_post( $post_data );
			
			$coupon_meta = array(
					'_wpc_store'                     => array($term_id),
					'_wpc_coupon_type'               => in_array('coupon', $item->aTypes) && $item->cCode ? 'code' : 'sale',
					//'start_datetime'                => '',
					'_wpc_expires'                   => strtotime($item->dtEndDate),
					'_wpc_exclusive'                 => '',
					'_wpc_coupon_type_code'          => in_array('coupon', $item->aTypes) && $item->cCode ? $item->cCode : '',
					//'_wpc_coupon_type_sale'          => '',
					'_wpc_coupon_type_printable_id'  => '',
					'_wpc_coupon_type_printable'     => '',
					'_wpc_destination_url'           => $deal_url,
					// meta keys for tracking
					'_wpc_used'                      => 0,
					'_wpc_percent_success'           => 200, // Change by client rule
					'_wpc_views'                     => 0,
					'_wpc_today'                     => '',
					'_wpc_vote_up'                   => 0,
					'_wpc_vote_down'                 => 0,
					'_wpc_sku'						 => $item->nLinkID
			);
			
			// Add link S3 of deal if API provide image url
			if ($item->cImage) {
				$linkS3_deal = upload_to_s3($item->cImage);
				$coupon_meta['_wpc_coupon_image'] = $linkS3_deal;
				$coupon_meta['_wpc_coupon_source_image'] = $item->cImage;
			// If not exist deal image we use screenshot image of merchant if exist
			} elseif ($link_store_screenshot) {
				$linkS3_deal = upload_to_s3($link_store_screenshot);
				$coupon_meta['_wpc_coupon_image'] = $linkS3_deal;
				$coupon_meta['_wpc_coupon_source_image'] = $link_store_screenshot;
			}
				
			foreach ( (array) $coupon_meta as $k => $v ){
				update_post_meta( $post_id, $k, $v );
			}
		}
		
		// END UPSERT COUPON
		
		// Insert to table coupon api if not exist
		if (! isset($coupon_api_results[$item->nCouponID])) {
			$wpdb->insert( $wpdb->prefix.'coupon_api', array('api_name' => $api_name, 'api_coupon_id' => $item->nCouponID, 'post_id' => $post_id), null );
		}
		
		// Set relationship between store and coupon
		wp_set_post_terms( $post_id, array($term_id), $store_tax );
		
		// Set relationship between category and coupon
		if ($item->aCategoriesV2) {
			$list_category_id = array();
			foreach ($item->aCategoriesV2 as $values) {
				$cat = custom_get_term( $values->cName, 'coupon_category' );
				if ($cat) {
					$list_category_id[] = $cat->term_id;
				}
			}
			
			wp_set_post_terms( $post_id, $list_category_id, 'coupon_category' );
		}
		
		// Set discount type for counpon
		set_coupon_discount_type($api_name, $post_id, (isset($item->aTypes) ? $item->aTypes : array()), $coupon_discount);
	}
	
	write_log('PROCESS - '. date('Y-m-d H:i:s')); // Write to log this process
	return "PROCESS";
}

add_shortcode( 'processfmtc', 'processfmtc' );


function processpopshop ($atts) {
	global $wpdb;
	$store_tax = 'coupon_store';
	$api_name = 'popshops';
	$params = shortcode_atts( array(
		'catalog_key' => '',
		'account_key' => '',
		'deal_type' => '-1',
		'deal_duration' => '3'
	), $atts );
	
	if (empty($atts['catalog_key']) || empty($atts['account_key'])) {
		return 'catalog_key or account_key was empty.';
	} elseif (! is_numeric($atts['deal_duration'])) {
		return 'deal_duration not valid.';
	}
 
	$end_on_min = date('Y/m/d');
	$end_on_max = date('Y/m/d',strtotime($end_on_min ."+". str_replace('-', '', $atts['deal_duration']) ." days"));
	$deal_type = $atts['deal_type'] == '-1' ? '' : '&deal_type='. $atts['deal_type'];
	$per_page = 100;
	$deal_url_api = "https://www.popshops.com/v3/deals.json?catalog=". $atts['catalog_key'] ."&account=". $atts['account_key'] ."&results_per_page=$per_page&site_wide=no&end_on_min=$end_on_min&end_on_max=$end_on_max$deal_type";

 	$response = get_response_from_api($deal_url_api);

 	// Get merchant type from API
 	$merchant_types = get_response_from_api("http://api.popshops.com/v3/merchant_types.json?account=". $atts['account_key']);
 	
	// Start check valid json
	$check_json = is_valid_json();
	if ($check_json !== TRUE) {
		return $check_json;
	}
	// End check valid json
	// Check if params(catalog_key, account_key.. not valid)
	if ($response->status != 200 || $merchant_types->status != 200)
		return "API fail.";
	
	$total_results = $response->results->deals->count;
	$total_pages = ceil($total_results / $per_page);
	
	// Get merchants in DB
	$merchant_db = get_merchant_db('popshops');
	
	// Get coupon discount type in DB
	$coupon_discount = get_coupon_discount_db($api_name);
	
	// Get deal types from API
	$deal_types = get_deal_types_from_api('popshops', "http://api.popshops.com/v3/deal_types.json");

	for ($p = 1; $p <= $total_pages; $p++) {
		if ($p != 1) {
		   // Get data next page if $p != 1 
			$response = get_response_from_api($deal_url_api. "&page=$p");
  		}
  	
		$deals = $response->results->deals->deal;
		
		// Get merchants in API
		$list_merchants_api = get_list_merchants_api($api_name, $deals, 'https://www.popshops.com/v3/merchants.json?catalog='. $atts['catalog_key'].'&account='.$atts['account_key'].'&merchant=');
		
		// Get coupon api from DB
		$coupon_api_results = get_list_coupon_api($api_name, $deals);
		
		foreach ($deals as $item) {			 
			// Check coupon exist on system by SKU
			$check_exist_coupon = check_exist_coupon_by_sku($item);
			// Not insert this coupon because duplicate
			if ($check_exist_coupon)
				continue;
			
			// Get merchant info from API
			$merchant_from_api = $list_merchants_api[$item->merchant];
		 	$merchant_name = $merchant_from_api->name;		 	
		 	$merchant_id = $merchant_from_api->id;
		 	$site_url = rtrim($merchant_from_api->site_url, '/');
		 	$url_logo = $merchant_from_api->logo_url;
		 	$api_url = $merchant_from_api->url;
		 	
			// START UPSERT STORE
			$t = custom_get_term($merchant_name, 'coupon_store', array('key' => '_wpc_store_site_url', 'value' => $site_url ));
			$t_data = array(
					'name' => $merchant_name,
					'slug' => sanitize_title($merchant_name),
					'parent' => 0,
					'description' => 'Insert from API'
			);
		
			if ($t) {		
				$term_id = $t->term_id;
				$term_meta_data = get_term_meta($term_id);
				foreach ($term_meta_data as $k => $v) {
					$term_meta_data_default[$k] = $v[0];
				}
				
				$merchant_api = array('api_name' => 'popshops',
						'merchant_id' => $merchant_id,
						'merchant_name' => $merchant_name,
						'site_url' => $site_url,
						'url_logo' => $url_logo,
						'api_url' => $api_url);
					
				$is_merchant_change = check_merchant_change($merchant_id, $merchant_db, $merchant_api);
				
				if ($is_merchant_change) {
					wp_update_term( $term_id , $store_tax, $t_data ); // Only update store when merchant from api update
					// If merchant update, we update relative term_meta fields
					$term_meta_data_default['_wpc_store_url'] = $api_url;
					$term_meta_data_default['_wpc_store_name'] = $merchant_name;
					$term_meta_data_default['_wpc_store_site_url'] = $site_url;
				}
			} else {
				//insert term
				$t = wp_insert_term($merchant_name, $store_tax, $t_data );
								
				if ( $t && ! is_wp_error( $t ) ) {
					$t = ( object ) $t;
					$term_id =  $t->term_id;
				} else {
					die("Insert term error");
				}
				
				$linkS3 = upload_to_s3($url_logo);
				$store_image_post_data = array();
				$store_image_post_data['post_title'] =  $linkS3;
				$store_image_post_data['guid'] =  $linkS3;
				$id_store_image_post = wp_insert_post( $store_image_post_data );
				
				// Set store category name
				$store_category_name = 'Other';
				if (isset($merchant_from_api->merchant_type) && $merchant_from_api->merchant_type) {
					$store_category_name = get_store_category_name($api_name, $merchant_from_api->merchant_type, $merchant_types->results->merchant_types->merchant_type);
				}
				$term_meta_data_default = array(
						'_wpc_store_url'       => $api_url,
						'_wpc_store_aff_url'   => '',
						'_wpc_store_name'      => $merchant_name,
						'_wpc_store_heading'   => '',
						'_wpc_is_featured'     => 'on',
						'_wpc_count_posts'     => 0,
						'_wpc_extra_info'      => '',
						'_wpc_store_image_id'  => $id_store_image_post,
						'_wpc_store_image'     => $linkS3,
						'_wpc_store_source_image' => $url_logo,
						'_wpc_store_site_url' => $site_url,
						'_wpc_store_category_name' => $store_category_name
				);
				add_post_meta($id_store_image_post, '_wp_attached_file', $linkS3, true);			
			}			
			
			if ( function_exists( 'update_term_meta' ) ) {
				foreach ( $term_meta_data_default as $k => $v ) {
					update_term_meta( $term_id, $k, $v );
				}
			}
			// END UPSERT STORE
		
			// START UPSERT COUPON
			$post_id = 0;
			$deal_content = get_deal_content($api_name, $item, $deal_types);
			
			if (isset($coupon_api_results[$item->id])) {
				$get_post = (array) get_post($coupon_api_results[$item->id]);
				if (! empty($get_post)) {
					$post_id = $get_post['ID'];
					// Only update post(coupon) when updated
					if ($get_post['post_title'] != $item->name || $get_post['post_content'] != $deal_content) {
						$get_post['post_title'] = $item->name;
						$get_post['post_content'] = $deal_content;
						wp_update_post($get_post);
					}
					
					$post_meta_data = get_post_meta($post_id);
					$coupon_meta_filter = array(
							'_wpc_coupon_type' 		=> $item->code ? 'code' : 'sale',
							'_wpc_expires'     		=> strtotime($item->end_on),
							'_wpc_coupon_type_code' => $item->code ? $item->code : '',
							'_wpc_destination_url'  => $item->url,
							'_wpc_coupon_source_image' => $item->image_url,
							'_wpc_store'	=> array($term_id)
					);
					
					// Update coupon meta in DB if coupon data from API update.
					check_coupon_meta_change($post_id, $post_meta_data, $coupon_meta_filter);
				}
			}
			
			if (! $post_id) {
				$post_data = array(
						'post_title'    => $item->name,
						'post_content'  => $deal_content,
						'post_excerpt'  => '',
						'post_date'     => date_i18n('Y-m-d H:i:s'),
						'post_author'   => $author,
						'post_type'     => 'coupon',
						'post_status'   => 'publish',
				);
				$post_id = wp_insert_post( $post_data );
				
				$coupon_meta = array(
						'_wpc_store'                     => array($term_id),
						'_wpc_coupon_type'               => $item->code ? 'code' : 'sale',
						//'start_datetime'                => '',
						'_wpc_expires'                   => strtotime($item->end_on),
						'_wpc_exclusive'                 => '',
						'_wpc_coupon_type_code'          => $item->code ? $item->code : '',
						//'_wpc_coupon_type_sale'          => '',
						'_wpc_coupon_type_printable_id'  => '',
						'_wpc_coupon_type_printable'     => '',
						'_wpc_destination_url'           => $item->url,
						// meta keys for tracking
						'_wpc_used'                      => 0,
						'_wpc_percent_success'           => 200,	// Change by client rule
						'_wpc_views'                     => 0,
						'_wpc_today'                     => '',
						'_wpc_vote_up'                   => 0,
						'_wpc_vote_down'                 => 0,
						'_wpc_sku'						 => $item->sku
				);
				
				// Add link S3 of deal if API provide image url
				if ($item->image_url) {
					$linkS3_deal = upload_to_s3($item->image_url);
					$coupon_meta['_wpc_coupon_image'] = $linkS3_deal;
					$coupon_meta['_wpc_coupon_source_image'] = $item->image_url;
				}
				
				foreach ( (array) $coupon_meta as $k => $v ){
					update_post_meta( $post_id, $k, $v );
				}
			}
			// END UPSERT COUPON
		
			// Insert to table coupon api if not exist
			if (! isset($coupon_api_results[$item->id])) {
				$wpdb->insert( $wpdb->prefix.'coupon_api', array('api_name' => $api_name, 'api_coupon_id' => $item->id, 'post_id' => $post_id), null );
			}
			
			// Set relationship between store and coupon
			wp_set_post_terms( $post_id, array($term_id), $store_tax );
			
			// Set discount type for counpon
			set_coupon_discount_type($api_name, $post_id, (isset($item->deal_type) ? explode(',', $item->deal_type) : array()), $coupon_discount);
		}
	}
	return "PROCESS";
}
add_shortcode( 'processpopshop', 'processpopshop');

function upsert_api_category($response_cat_api, $api_name) {
	foreach ($response_cat_api as $value) {
		if (! custom_get_term( $value['cName'], 'coupon_category' )) {
			wp_insert_term($value['cName'], 'coupon_category',array('description'=> 'Insert from API'));
		}
	}
}

function is_valid_json() {
	switch (json_last_error()) {
		case JSON_ERROR_NONE:
			return TRUE;
			break;
		case JSON_ERROR_DEPTH:
			return ' - Maximum stack depth exceeded';
			break;
		case JSON_ERROR_STATE_MISMATCH:
			return ' - Underflow or the modes mismatch';
			break;
		case JSON_ERROR_CTRL_CHAR:
			return ' - Unexpected control character found';
			break;
		case JSON_ERROR_SYNTAX:
			return ' - Syntax error, malformed JSON';
			break;
		case JSON_ERROR_UTF8:
			return ' - Malformed UTF-8 characters, possibly incorrectly encoded';
			break;
		default:
			return ' - Unknown error';
			break;
	}
}

function compress($source, $destination, $quality) {

	$info = getimagesize($source);

	if ($info['mime'] == 'image/jpeg') 
		$image = imagecreatefromjpeg($source);

	elseif ($info['mime'] == 'image/gif') 
		$image = imagecreatefromgif($source);

	elseif ($info['mime'] == 'image/png') 
		$image = imagecreatefrompng($source);

	imagejpeg($image, $destination, $quality);

	return $destination;
}

function upload_to_s3($linkimage) {
	if($linkimage !='' ) {
		require($_SERVER['DOCUMENT_ROOT'].'/aws/s3_config.php');	
		$client = S3Client::factory(array(
	    	'key'    =>  AWS_S3_ACCESSKEY,
	    	'secret' =>  AWS_S3_SECRETKEY
		));
 
		$client->registerStreamWrapper();
		
		$actual_image_name_on_aws = time();
		$extension = '.'.getExtension($linkimage);
		if( copy($linkimage, 's3://'.AWS_S3_BUCKET.'/'.$actual_image_name_on_aws.$extension) ) {
			$ori_image = 'http://'.AWS_S3_BUCKET.'.s3.amazonaws.com/'.$actual_image_name_on_aws.$extension;
			$compress_image = $actual_image_name_on_aws.'-compress'.$extension;
			copy($ori_image, $_SERVER['DOCUMENT_ROOT'].'/wp-content/tmps3/'.$compress_image);
			compress($_SERVER['DOCUMENT_ROOT'].'/wp-content/tmps3/'.$compress_image, $_SERVER['DOCUMENT_ROOT'].'/wp-content/tmps3/'.$compress_image, 70);
			
		 	if(copy($_SERVER['DOCUMENT_ROOT'].'/wp-content/tmps3/'.$compress_image, 's3://'.AWS_S3_BUCKET.'/'.$compress_image)) {
		 		$client->copyObject(array(
					'Bucket' => AWS_S3_BUCKET,
					'CopySource' => AWS_S3_BUCKET.'/'.$compress_image,
					'Key' => $compress_image,
					'CacheControl'=> 'max-age=2592000,public',
					'Expires' => '2034-01-01T00:00:00Z',
					'ContentType' => 'image/'.getExtension($linkimage),
					'MetadataDirective' => 'REPLACE'
				));
				unlink($_SERVER['DOCUMENT_ROOT'].'/wp-content/tmps3/'.$compress_image);
		 	}
			
			$s3file='http://'.AWS_S3_BUCKET.'.s3.amazonaws.com/'.$compress_image;
			return $s3file;
		}else {
			return false;
		}
	}
}

function delete_to_s3($filename) {
	require($_SERVER['DOCUMENT_ROOT'].'/aws/s3_config.php');	
	$client = S3Client::factory(array(
    	'key'    =>  AWS_S3_ACCESSKEY,
    	'secret' =>  AWS_S3_SECRETKEY
	));
	$client->registerStreamWrapper();
	if(unlink('s3://'.AWS_S3_BUCKET.'/'.$filename))
		return true;
	else
		return false;	
}

function get_thumbnail_s3($class_css, $post_id) {
	global $wpdb;
	$result = $wpdb->get_results('SELECT meta_value FROM `'.$wpdb->prefix.'postmeta` WHERE post_id = '.$post_id);
	if( strpos($result[0]->meta_value, 's3.amazonaws.com') !== false) {
		$linkS3 = $result[0]->meta_value;
		return '<img class="'.$class_css.'" src="'.$linkS3.'" alt="">';
	}else {
		return false;
	}
}

function getExtension($str) 
{
         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
}



// DEFINE function write log
if ( ! function_exists('write_log')) {
	function write_log ( $log )  {
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, true ) );
		} else {
			error_log( $log );
		}
	}
}
/**
 * featuredPost
 * @param int $number
 * @param string $tag
 */

function featuredPost($number,$tag) {
	$posts = array( 'posts_per_page' => $number,
		'post_type' => 'post',
		'post_status' => 'publish',
		'tag' => $tag,
		'order_by' => 'date' );
	return get_posts( $posts );
}

/**
 * HomedPost
 */

function HomedPost() {
	$args1 = array( 'posts_per_page' => 7, 
	'post_type' => 'post',
	'post_status' => 'publish',
	'order_by' => 'date' );
	$posts = get_posts( $args1 );
	$featured_post = array_shift( $posts );
	$thumb = get_the_post_thumbnail($featured_post->ID, array( 620, 300));
	if(strlen($featured_post->post_content) > 250)
	{
		$desc =substr($featured_post->post_content,0,strpos($featured_post->post_content, ' ', 250));
	}else{
		$desc = $featured_post->post_content;
	}
	
	$postdate = GetTimePost($featured_post->post_date);
	$permalink = get_permalink($featured_post);
	$author_name = get_the_author_meta('display_name',$featured_post->post_author);
	$author_url = get_author_posts_url($featured_post->post_author,get_the_author_meta('user_nicename',$featured_post->post_author));
	$str = '<aside class="bloghomepage"><a class="lastpost_title" href="'.$permalink.'">'.get_the_title($featured_post).'</a>
	<div class="post-thum"><a href="'.$permalink.'">'.$thumb.'</a></div><div class="post-img"><a href="'.$permalink.'">'.$desc.'</a></div>';
	$str .= '<div class="post-meta"><div class="post-meta-data"><p class="meta-line-2"><span class="author-name">By ';
	$str .= '<a href="'.$author_url.'" class="bloghomeauthor" title="'.$author_name.'" rel="author">'.$author_name.',</a>';
	$str .= '<span class="comment-number"> '.$postdate.'</span>';
	$str .= '</span></p></div></div>';
	$str .= '</aside>';
	$str .= '<aside id="" class="recent-posts-5 widget widget_recent_entries"><h3 class="widget-title">Popular Posts</h3>';
	$str .= '<ul>';
	foreach ( $posts as $post ) {
		$str .='<li><a href="'.get_permalink($post).'">'.get_the_title($post).'</a></li>';
	}
	$str .= '</ul>';
	$str .= '</aside>';
	echo($str);
}

function enqueue_scripts_styles_init() {
	wp_enqueue_script( 'ajax-script', get_stylesheet_directory_uri().'/assets/js/script.js', array('jquery'), 1.0 ); 
	wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) ); 
}
add_action('init', 'enqueue_scripts_styles_init');
add_action( 'wp_ajax_ajax_action', 'HomedPost' ); 
add_action( 'wp_ajax_nopriv_ajax_action', 'HomedPost' ); 

/**
 * GetTimePost
 * @param datetime $date
 * Return string
 */

function GetTimePost($date)
{
	if(empty($date)) {
		return "No date provided";
	}
	
	$periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	$lengths         = array("60","60","24","7","4.35","12","10");
	
	$now             = time();
	$unix_date         = strtotime($date);
	
	// check validity of date
	if(empty($unix_date)) {    
		return "N/A";
	}

	// is it future date or past date
	if($now > $unix_date) {    
		$difference     = $now - $unix_date;
		$tense         = "ago";
		
	} else {
		$difference     = $unix_date - $now;
		$tense         = "";
	}
	
	for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		$difference /= $lengths[$j];
	}
	
	$difference = round($difference);
	
	if($difference != 1) {
		$periods[$j].= "s";
	}
	
	return "$difference $periods[$j] {$tense}";
}

/**
 * get_response_from_api
 * @param string $url
 * @param boolean $json_decode_array
 * @return json decode result from api
 */
function get_response_from_api($url, $json_decode_array = FALSE) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	$response = curl_exec($ch);
	curl_close($ch);

	return json_decode($response, $json_decode_array);
}

/*
 * get_merchant_db
 * @param string $api_name
 * @return array merchant saved of api
 */
function get_merchant_db($api_name = '') {
	global $wpdb;
	return $wpdb->get_results("SELECT id, merchant_id, merchant_name, site_url, url_logo, api_url FROM `" .$wpdb->prefix. "coupon_merchant` WHERE api_name = '$api_name'");
}

/*
 * check_exist_merchart
 * @param int $merchant_id
 * @param array $list_merchant
 * @return array or false 
 */
function check_exist_merchart($merchant_id = 0, $list_merchant = array()) {
	foreach ($list_merchant as $item) {
		if ($item->merchant_id == $merchant_id) {
			return $item;
		}
	}
	
	return false;
}

/*
 * upsert_merchant 
 * @param array $merchant_data
 * @param int $merchant_id
 */
function upsert_merchant($merchant_data , $merchant_id = FALSE) {
	// Check merchant id in $merchant_data before continue
	if (! isset($merchant_data['merchant_id']) || ! $merchant_data['merchant_id'])
		return false;
	
	global $wpdb;
	if ($merchant_id) {
		$find_by_merchant_id = $wpdb->get_results("SELECT id FROM `" .$wpdb->prefix. "coupon_merchant` WHERE merchant_id = '$merchant_id'");
		if (! empty($find_by_merchant_id)) {
			$wpdb->update( $wpdb->prefix. "coupon_merchant", $merchant_data, array('merchant_id' => $merchant_id) );
		}
	} else {
		$find_by_merchant_id = $wpdb->get_results("SELECT id FROM `" .$wpdb->prefix. "coupon_merchant` WHERE api_name = '". $merchant_data['api_name'] ."' AND merchant_id = ". $merchant_data['merchant_id']);
		if (! empty($find_by_merchant_id)) {
			$wpdb->update( $wpdb->prefix. "coupon_merchant", $merchant_data, array('id' => $find_by_merchant_id[0]->id) );
		} else {
			$wpdb->insert( $wpdb->prefix. "coupon_merchant", $merchant_data, null );
		}		
 	}
}

/*
 * check_merchant_change: function return true if merchant on DB and API different, update mechart at the same time. Return false   
 * @param int $merchant_id
 * @param array $merchant_db: list merchant in DB
 * @param array $merchant_api
 * @return boolean
 */
function check_merchant_change($merchant_id, $merchant_db, $merchant_api) {
	$get_exist_merchant = check_exist_merchart($merchant_id, $merchant_db);
	if($get_exist_merchant) {
		$merchant_info = (array) $get_exist_merchant;
		if( ($merchant_info['merchant_name'] != $merchant_api['merchant_name']) || ($merchant_info['site_url'] != $merchant_api['site_url']) ||
		($merchant_info['url_logo'] != $merchant_api['url_logo']) || ($merchant_info['api_url'] != $merchant_api['api_url']) ) {
			// Update merchant DB
			upsert_merchant($merchant_api, $merchant_id);
			return true;
		}
		else {
			return false;
		}
	}else {
		// Insert merchant DB
		upsert_merchant($merchant_api);
		return false;
	}	 
}

/*
 * check_coupon_meta_change: update coupon meta when data from api update
 * @param int $post_id
 * @param array $coupon_meta
 * $param array $coupon_api
 */
function check_coupon_meta_change($post_id, $coupon_meta, $coupon_api) {
	if ($coupon_meta['_wpc_coupon_type'][0] != $coupon_api['_wpc_coupon_type'])
		update_post_meta( $post_id, '_wpc_coupon_type', $coupon_api['_wpc_coupon_type'] );
	if ($coupon_meta['_wpc_expires'][0] != $coupon_api['_wpc_expires'])
		update_post_meta( $post_id, '_wpc_expires', $coupon_api['_wpc_expires'] );
	if ($coupon_meta['_wpc_coupon_type_code'][0] != $coupon_api['_wpc_coupon_type_code'])
		update_post_meta( $post_id, '_wpc_coupon_type_code', $coupon_api['_wpc_coupon_type_code'] );
	if ($coupon_meta['_wpc_destination_url'][0] != $coupon_api['_wpc_destination_url'])
		update_post_meta( $post_id, '_wpc_destination_url', $coupon_api['_wpc_destination_url'] );
	if (unserialize($coupon_meta['_wpc_store'][0]) != $coupon_api['_wpc_store']) {
		update_post_meta( $post_id, '_wpc_store', $coupon_api['_wpc_store'] );
		// Set relation ship between coupon and store
		wp_set_post_terms( $post_id, $coupon_api['_wpc_store'], 'coupon_store' );
	}
	
	// Update deal image if update
	if ($coupon_api['_wpc_coupon_source_image']) {
		// Update if exist and different api deal source image 
		if (isset($coupon_meta['_wpc_coupon_source_image'][0]) && $coupon_meta['_wpc_coupon_source_image'][0] != $coupon_api['_wpc_coupon_source_image']) {
			// Delete current image on S3
			$filename = substr($coupon_meta['_wpc_coupon_image'][0], (strrpos($coupon_meta['_wpc_coupon_image'][0], '/') + 1));
			delete_to_s3($filename);
			// Upload image to S3
			$linkS3 = upload_to_s3($coupon_api['_wpc_coupon_source_image']);
			update_post_meta( $post_id, '_wpc_coupon_image', $linkS3 );
			update_post_meta( $post_id, '_wpc_coupon_source_image', $coupon_api['_wpc_coupon_source_image'] );
		// Add deal image to post(coupon) meta if "_wpc_coupon_source_image" not exist
		} elseif (! isset($coupon_meta['_wpc_coupon_source_image'][0])) {
			$linkS3 = upload_to_s3($coupon_api['_wpc_coupon_source_image']);
			update_post_meta( $post_id, '_wpc_coupon_image', $linkS3 );
			update_post_meta( $post_id, '_wpc_coupon_source_image', $coupon_api['_wpc_coupon_source_image'] );
		}
	}
}

/*
 * get_list_merchants_api
 * param string $api_name 
 * param array $deals
 * return list_merchants
 */
function get_list_merchants_api($api_name, $deals, $query_api) {
	if (! empty($deals)) {
		$merchant_ids = array();
		foreach ($deals as $item) {
			$merchant_id = $api_name == 'fmtc' ? $item->nMerchantID : $item->merchant;
			if (! in_array($merchant_id, $merchant_ids)) {
				$merchant_ids[] = $merchant_id;
			}
		}
		$merchants_api = get_response_from_api($query_api. implode(',',$merchant_ids));
		// Start check valid json
		$check_json = is_valid_json();
		if ($check_json !== TRUE) {
			echo $check_json;
			die();
		}
		// End check valid json
		
		// Return list merchants with key is merchant id
		$results = array();
		$list_merchants_api = $api_name == 'fmtc' ? $merchants_api : $merchants_api->results->merchants->merchant;
		foreach ($list_merchants_api as $v) {
			$merchant_id = $api_name == 'fmtc' ? $v->nMerchantID : $v->id;
			$results[$merchant_id] = $v;
		}
		
		return $results;
	} else {
		return array();
	}
}

function get_deal_image($post_id) {
	$post_meta = get_post_meta($post_id, '_wpc_coupon_image');
	if(!empty($post_meta)) {
		return '<img class="coupon-img-s3" src="'.$post_meta[0].'"/>';
	} else {
		return '<img class="coupon-img-s3" src="http://www.offer.me/wp-content/themes/wp-coupon/assets/images/deal_img_default.jpg"/>';
	}
}

/*
 * get_list_coupon_api
 * @param string $api_name
 * @param array $deals
 * @return array - key is coupon id and value is post(coupon) id
 */
function get_list_coupon_api($api_name, $deals) {
	if (! empty($deals)) {
		global $wpdb;
		$coupon_ids = array();
		foreach ($deals as $item) {
			$coupon_ids[] = $api_name == 'fmtc' ? $item->nCouponID : $item->id;
		}
		$coupons = $wpdb->get_results("SELECT post_id, api_coupon_id from ". $wpdb->prefix ."coupon_api WHERE api_name = '$api_name'
				AND api_coupon_id IN (". implode(',', $coupon_ids) .") AND post_id IS NOT NULL");
		
		
		// Return list coupon with key is coupon id
		$results = array();
		foreach ($coupons as $v) {
			$results[$v->api_coupon_id] = $v->post_id;
		}
		
		return $results;
	} else {
		return array();
	}
}

/*
 * get_coupon_discount_db
 * @param $api_name
 * @return array
 */
function get_coupon_discount_db($api_name) {
	global $wpdb;
	return $wpdb->get_results("SELECT post_id, discount_type_id FROM `" .$wpdb->prefix. "coupon_discount_type` WHERE api_name = '$api_name'");
}

/*
 * is_same_coupon_discount
 * @param array $coupon_discount
 * @param int $post_id
 * @param array $discount_ids
 * @return boolean
 */
function is_same_coupon_discount($coupon_discount, $post_id, $discount_ids) {
	$disc_ids_db = array();
	foreach ($coupon_discount as $cd) {
		if ($cd->post_id == $post_id)
			$disc_ids_db[] = $cd->discount_type_id;
	}
	// Sort array for compare
	sort($disc_ids_db);
	sort($discount_ids);

	if ($disc_ids_db == $discount_ids) {
		return true;
	}

	return false;
}

/*
 * set_coupon_discount_type
 * @param string $api_name
 * @param int $post_id
 * @param array $deal_type
 * @param array $coupon_discount
 */
function set_coupon_discount_type($api_name, $post_id, $deal_type, $coupon_discount) {
	// If exist discount type by popshops, we do nothing
	global $wpdb;
	$exist_discount_popshops = $wpdb->get_row("SELECT id FROM `" .$wpdb->prefix. "coupon_discount_type` WHERE api_name = 'popshops' AND post_id = $post_id");
	if ($exist_discount_popshops)
		return false;
	
	$free_ship_key = $api_name == 'fmtc' ? 'freeshipping' : '1';
	$gift_key = $api_name == 'fmtc' ? 'gift' : '3';
	$dollar_off_key = $api_name == 'fmtc' ? 'dollar' : '6';
	$percent_off_key = $api_name == 'fmtc' ? 'percent' : '5';
	$format_deal_type = array(
			$free_ship_key => array(
					'discount_id' => 1,
					'discount_name' => 'Free Shipping'
			),
			$gift_key => array(
					'discount_id' => 2,
					'discount_name' => 'Free Gift'
			),
			$dollar_off_key => array(
					'discount_id' => 3,
					'discount_name' => '$ Off'
			),
			$percent_off_key => array(
					'discount_id' => 4,
					'discount_name' => '% Off'
			)
	);

	$discount_ids = array();
	$discount_names = array();

	// Set discount type other for coupon has empty deal type
	if (empty($deal_type)) {
		$discount_ids[] = 9;
		$discount_names[] = 'Other';
	}

	foreach ($deal_type as $v) {
		if (isset($format_deal_type[$v])) {
			if (! in_array($format_deal_type[$v]['discount_id'], $discount_ids)) {
				$discount_ids[] = $format_deal_type[$v]['discount_id'];
				$discount_names[] = $format_deal_type[$v]['discount_name'];
			}
		} else {
			if (! in_array(9, $discount_ids)) {
				$discount_ids[] = 9;
				$discount_names[] = 'Other';
			}
		}
	}

	// Update discount type for coupon
	if (! is_same_coupon_discount($coupon_discount, $post_id, $discount_ids)) {
		$wpdb->get_results("DELETE FROM " .$wpdb->prefix. "coupon_discount_type WHERE post_id = '$post_id'");
		$sql = "INSERT INTO " .$wpdb->prefix. "coupon_discount_type (api_name, post_id, discount_type_id, discount_type_name) VALUES";
		$row = array();
		foreach ($discount_ids as $k => $v) {
			$row[] = "('$api_name', $post_id, $v, '". $discount_names[$k] ."')";
		}
		$row = implode(',', $row);
		$sql .= $row;
		$wpdb->get_results($sql);
	}
}

/*
 * get_fmtc_logo_url
 * @param array $logo_obj
 * @param string $type logo|screenshot, default 'logo'
 * @return string
 */
function get_fmtc_logo_url($logo_obj, $type = 'logo') {
	$result = $type == 'logo' ? 'http://www.offer.me/wp-content/themes/wp-coupon/assets/images/store_logo_default.jpg' : 'http://www.offer.me/wp-content/themes/wp-coupon/assets/images/deal_img_default.jpg';
	foreach ($logo_obj as $v) {
		if ($type == 'logo' && $v->cSize == '120x60') {
			$result = $v->cURL;
			break;
		} elseif ($type == 'screenshot' && $v->cSize == '600x450') {
			$result = $v->cURL;
			break;
		}
	}
	return $result;
}

/*
 * get_deal_types_from_api
 * @param string $api_name
 * @param string $query_api
 * @return array 
 */
function get_deal_types_from_api($api_name, $query_api) {
	$deal_type_api = get_response_from_api($query_api);
	// Start check valid json
	$check_json = is_valid_json();
	if ($check_json !== TRUE) {
		echo $check_json;
		die();
	}
	
	$result = array();
	$deal_type_list = $api_name == 'fmtc' ? $deal_type_api : $deal_type_api->results->deal_types->deal_type;
	foreach($deal_type_list as $v) {
		$key = $api_name == 'fmtc' ? $v->cSlug : $v->id;
		$deal_type_name = $api_name == 'fmtc' ? $v->cName : $v->name;
		$result[$key] = $deal_type_name;
	}
	
	return $result;
}

/*
 * get_deal_content: priority - restriction -> deal type -> deal name
 * @param string $api_name
 * @param object $deal
 * @param array $deal_types_list
 * @return string 
 */
function get_deal_content($api_name, $deal, $deal_types_list) {
	$restriction = $api_name == 'fmtc' ? $deal->cRestrictions : isset($deal->restrictions) ? $deal->restrictions : '';
	
	if ($restriction)
		return $restriction;
	
	$deal_types = $api_name == 'fmtc' ? $deal->aTypes : (isset($deal->deal_type) ? explode(',', $deal->deal_type) : array());
	
	if (! empty($deal_types)) {
		$result = array();
		foreach ($deal_types as $key) {
			if (array_key_exists($key, $deal_types_list))
				$result[] = $deal_types_list[$key];
		}
		return implode(', ', $result);
	}
	
	return $api_name == 'fmtc' ? $deal->cLabel : $deal->name;
}

/*
 * super_unique: Get unique array
 * @param array 
 * @return array 
 */
function super_unique($array)
{
  $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

  foreach ($result as $key => $value)
  {
    if ( is_array($value) )
    {
      $result[$key] = super_unique($value);
    }
  }
  return $result;
}

/*
 * get_available_categories
 * @param array $coupons
 * @return array
 */
function get_available_categories($coupons) {
	$result = array();
	foreach ($coupons as $coupon) {
		$terms = get_the_terms( $coupon->ID, 'coupon_category' );
		if ($terms) {
			foreach ( $terms as $term ) {
				if (array_key_exists($term->term_id, $result)) {
					$result[$term->term_id]['count']++;
					if (! in_array($coupon->ID, $result[$term->term_id]['coupon_ids'])) {
						$result[$term->term_id]['coupon_ids'][] = $coupon->ID;
					}
				} else {
					$result[$term->term_id] = array(
						'name'	=> $term->name,
						'count'	=> 1,
						'coupon_ids' => array($coupon->ID)
					);
				}
			}
		}
		
	}
	uasort($result, 'sort_by_count');

	return $result;
}

/*
 * get_available_store
 * @param array $coupons
 * @return array
 */
function get_available_store($coupons) {
	$result = array();
	
	foreach ($coupons as $coupon) {
		$store_id = WPCoupon_Coupon($coupon)->get_store_id();
		$terms = get_term_by('id',$store_id,'coupon_store');
		if ($terms) {
			if (array_key_exists($terms->term_id, $result)) {
				$result[$terms->term_id]['count']++;
				if (! in_array($coupon->ID, $result[$terms->term_id]['coupon_ids'])) {
					$result[$terms->term_id]['coupon_ids'][] = $coupon->ID;
				}
			} else {
				$result[$terms->term_id] = array(
					'name'	=> $terms->name,
					'count'	=> 1,
					'coupon_ids' => array($coupon->ID)
				);
			}
		}
				
	}
	uasort($result, 'sort_by_count');

	return $result;
}

/*
 * get_available_discount_type
 * @param array $coupons
 * @return array
 */
function get_available_discount_type($coupons) {
	$result = array();
	$post_ids = array();
	$post_ids_added = array();
	foreach ($coupons as $coupon) {
		$post_ids[] = $coupon->ID;
	}
	global $wpdb;
	$discount_type_results = $wpdb->get_results("SELECT post_id, discount_type_id, discount_type_name FROM `" .$wpdb->prefix. "coupon_discount_type` WHERE post_id IN(". implode(',', $post_ids) .") ORDER BY discount_type_id");
	if (empty($discount_type_results))
	{
		$result[9] = array(
			'name'	=> 'Other',
			'count'	=> count($post_ids),
			'coupon_ids' => $post_ids
		);
	}
	foreach ($discount_type_results as $v) {
		if (! in_array($v->post_id, $post_ids_added) && ! in_array($v->post_id, $post_ids_added)) {
			$post_ids_added[] = $v->post_id;
		}
		if (array_key_exists($v->discount_type_id, $result)) {
			if (! in_array($v->post_id, $result[$v->discount_type_id]['coupon_ids'])) {
				$result[$v->discount_type_id]['count']++;
				$result[$v->discount_type_id]['coupon_ids'][] = $v->post_id;
			}
		} else {
			$result[$v->discount_type_id] = array(
					'name'	=> $v->discount_type_name,
					'count'	=> 1,
					'coupon_ids' => array($v->post_id)
			);
		}
	}

	$diff_post_ids = array_diff($post_ids,$post_ids_added);;
	if (! empty($discount_type_results) && ! empty($diff_post_ids)) {
		$not_added_post_ids = array_values($diff_post_ids);
		if (isset($result[9])) {
			$result[9]['count'] += count($not_added_post_ids);
			$result[9]['coupon_ids'] = array_merge($result[9]['coupon_ids'], $not_added_post_ids);
		} else {
			$result[9] = array(
					'name'	=> 'Other',
					'count'	=> count($not_added_post_ids),
					'coupon_ids' => $not_added_post_ids
			);
		}
	}
	
	return $result;
}

/*
 * get_coupon_ids_by_type
 * @param array $coupons
 * @return array
 */
function get_coupon_ids_by_type($coupons) {
	$result = array(
		'code' => array(),
		'sale' => array(),
		'print' => array()
	);
	foreach ($coupons as $coupon) {
		$coupon_type = wpcoupon_get_coupon_type($coupon->ID);
		if (array_key_exists($coupon_type, $result)) {
			$result[$coupon_type][] = $coupon->ID;
		}
	}
	
	return $result;
}

function sort_by_count($a, $b) {
	if($a['count'] == $b['count']) {
		return 0;
	}
	return ($a['count'] > $b['count']) ? -1 : 1;
}

/*
 * get_store_category_name
 * @param string $api_name
 * @param string|int $merchant_type_id
 * @param array $list_merchant_type
 * @return string
 */
function get_store_category_name($api_name, $merchant_type_id, $list_merchant_type) {
	$result = 'Other';
	foreach ($list_merchant_type as $item) {
		$current_merchant_type_id = $api_name == 'fmtc' ? $item->nCategoryID : $item->id;
		if ($current_merchant_type_id == $merchant_type_id) {
			return $api_name == 'fmtc' ? $item->cName : $item->name;
		}
	}
	return $result;
}

/*
 * check_exist_coupon_by_sku
 * @param coupon_info
 * @return boolean
 */
function check_exist_coupon_by_sku($coupon_info) {
	$result = false;
	if (isset($coupon_info->sku) && $coupon_info->sku) {
		global $wpdb;
		$record = $wpdb->get_row("SELECT post_id FROM `" .$wpdb->prefix. "postmeta` WHERE meta_key = '_wpc_sku' AND meta_value = '". $coupon_info->sku ."' ");
		
		if ($record) {
			$result = true;
			
			// Replace coupon image if coupon_info have attribute "image_url" and coupon image on system was default image
			if (isset($coupon_info->image_url) && $coupon_info->image_url) {
				$post_meta = get_post_meta($record->post_id);
				
				if (! empty($post_meta) && isset($post_meta['_wpc_coupon_source_image']) && isset($post_meta['_wpc_coupon_image']) && $post_meta['_wpc_coupon_source_image'][0] == 'http://www.offer.me/wp-content/themes/wp-coupon/assets/images/deal_img_default.jpg') {
					// Delete current image on S3
					$filename = substr($post_meta['_wpc_coupon_image'][0], (strrpos($post_meta['_wpc_coupon_image'][0], '/') + 1));
					delete_to_s3($filename);
					// Upload image to S3
					$linkS3 = upload_to_s3($coupon_info->image_url);
					// Update post meta
					update_post_meta($record->post_id, '_wpc_coupon_image', $linkS3);
					update_post_meta($record->post_id, '_wpc_coupon_source_image', $coupon_info->image_url);
				}
			}
			
			// Replace discount type by popshops
			set_coupon_discount_type('popshops', $record->post_id, (isset($coupon_info->deal_type) ? explode(',', $coupon_info->deal_type) : array()), array());
		}
	}
	
	return $result;
}

/*
 * get_fmtc_url
 * @param $item
 * @param $type store|deal
 * @return string
 */
function get_fmtc_url($item, $type) {
	$result = '';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	
	$affiliateLink = $item->cAffiliateURL;
	curl_setopt($ch, CURLOPT_URL, $affiliateLink);
	$response = curl_exec($ch);
	$string_err = "Invalid Publisher Code, Offer, or Publisher - Advertiser Partnership Status";
	if (! strpos($response, $string_err))
		$result = $affiliateLink;
	
	if (! $result) {
		$skimlinksURL = $item->cSkimlinksURL;
		curl_setopt($ch, CURLOPT_URL, $skimlinksURL);
		$response = curl_exec($ch);
		if (! strpos($response, $string_err))
			$result = $skimlinksURL;
	}
	
	if (! $result) {
		$fmtcURL = $item->cFMTCURL;
		curl_setopt($ch, CURLOPT_URL, $fmtcURL);
		$response = curl_exec($ch);
		if (! strpos($response, $string_err))
			$result = $fmtcURL;
	}
	
	if (! $result)
		$result = $type == 'store' ? $item->cHomepageURL : $item->cDirectURL;
	
	curl_close($ch);
	
	return $result;
}

/*
 * archive_coupon: Archive coupon has expires older than 31 days.
 */
function archive_coupon() {
	$older_than_day = 31;
	$archive_day = new DateTime(date('Y-m-d', strtotime("-$older_than_day days")));
	$archive_day->setTime(0, 0);
	
	// Get posts has expires older than 31 days
	global $wp_query;
	$args = array(
			'post_type'      => 'coupon',
			'post_status'    => 'publish',
			'meta_key'       => '_wpc_expires',
			'meta_type'		 => 'NUMERIC',
			'posts_per_page' => 1000,
			'paged' => 1,
			'meta_query'     => array(
					'relation' => 'AND',
					array(
		                'key' => '_wpc_expires',
		                'compare' => 'EXISTS',
		            ),
		            array(
		                'key' => '_wpc_expires',
		                'type' => 'NUMERIC',
		                'value' => 0,
		                'compare' => '>',
		            ),
		            array(
		                'key' => '_wpc_expires',
		                'value' => $archive_day->getTimestamp(),
		                'type' => 'NUMERIC',
		                'compare' => '<',
		            ),
	
			),
			'orderby'         => 'meta_value',
			'order'           => 'desc'
	);
	
	$wp_query = new WP_Query( $args );
	$post_results = $wp_query->get_posts();
	
	if (! empty($post_results)) {
		foreach ($post_results as $p) {
			$p_id = $p->ID;
			// Delete coupon image from S3
			$image_deal_from_s3 = get_post_meta( $p_id, '_wpc_coupon_image', true );
			if ($image_deal_from_s3) {
				$filename = substr($image_deal_from_s3, (strrpos($image_deal_from_s3, '/') + 1));
				delete_to_s3($filename);
			}
			// Archive this coupon
			wp_delete_post( $p_id, true );
		}
	}
	
	return "DONE.";
}
add_shortcode( 'archive_coupon', 'archive_coupon');

/*
 * get_stores_with_category_name
 * @param $stores
 * @return array
 */
function get_stores_with_category_name($stores = array()) {
	$result = array();
	
	if (! empty($stores)) {
		$store_ids = array_map(create_function('$st', 'return $st->term_id;'), $stores);
		global $wpdb;
		
		$meta_store_category_name = $wpdb->get_results("SELECT term_id, meta_value FROM `" .$wpdb->prefix. "termmeta` WHERE meta_key = '_wpc_store_category_name' AND meta_value IS NOT NULL" );
		foreach ($stores as $store) {
			$store_id = $store->term_id;
			$needed_object = get_object_in_array($meta_store_category_name, 'term_id', $store_id);
			if ($needed_object) {
				if (array_key_exists($needed_object->meta_value, $result)) {
					$result[$needed_object->meta_value][] = $store;
				} else {
					$result[$needed_object->meta_value] = array($store);
				}
			} else {
				if (array_key_exists('Other', $result)) {
					$result['Other'][] = $store;
				} else {
					$result['Other'] = array($store);
				}
			}
		}
		ksort($result);
	}
	
	return $result;
}

/*
 * get_object_in_array
 * @param array $array
 * @param string $key
 * @param string|int $value
 * @return object|false
 */
function get_object_in_array($array, $key, $value) {
	if(! empty($array) && is_array($array) && $key && $value) {
		foreach ($array as $item) {
			if (isset($item->{$key}) && $item->{$key} == $value) {
				return $item;
			}
		}
	}
	
	return false;
}

/*
 * get_num_stores_and_coupons
 */
function get_num_stores_and_coupons() {
	global $wpdb;
	$GLOBALS['total_stores'] = 0;
	$GLOBALS['total_coupons'] = 0;
	$GLOBALS['last_coupon_update'] = null;

	$store_result = $wpdb->get_row("SELECT COUNT(DISTINCT t.term_id) as total FROM `". $wpdb->prefix ."terms` as t JOIN `". $wpdb->prefix ."term_taxonomy` as tx ON (tx.taxonomy = 'coupon_store' AND tx.term_id = t.term_id)");
	$coupon_result = $wpdb->get_row("SELECT COUNT(p.ID) as total FROM `". $wpdb->prefix ."posts` as p WHERE p.post_type = 'coupon' AND p.post_status = 'publish'");
	$last_coupon_update = $wpdb->get_row("SELECT p.post_date FROM `". $wpdb->prefix ."posts` as p WHERE p.post_type = 'coupon' AND p.post_status = 'publish' ORDER BY p.post_date DESC LIMIT 0,1");
	if ($store_result)
		$GLOBALS['total_stores'] = $store_result->total;
		if ($coupon_result)
			$GLOBALS['total_coupons'] = $coupon_result->total;
			if ($last_coupon_update)
				$GLOBALS['last_coupon_update'] = date("F j, Y", strtotime($last_coupon_update->post_date));
}
get_num_stores_and_coupons();

// Admin logout redirect to home page
function admin_logout_redirect(){
	if (is_super_admin(get_current_user_id())) {
		wp_redirect( home_url() );
		exit();
	}
}
add_action('wp_logout','admin_logout_redirect');
