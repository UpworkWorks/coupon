<?php
get_header();
$cate_id =  get_queried_object_id();
/**
* Hooked
*
* @see: wpcoupon_store_coupons_filter -  15
* @see wpcoupon_store_coupons_filter
* @since 1.0.0
*/
	

	/**
	* get coupons of this store
	*/
global $wp_query;
$coupons =  $wp_query->posts;
$term_id =  get_queried_object_id();		            
$number_active = 100;		          
//  $coupons = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'active' );
//   $coupons_unpopular = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'unpopular' );
//	$coupons_expires = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'expires' );

	global $wp_rewrite;
$paged =  wpcoupon_get_paged();
$args = array(
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'coupon_category',
			'field'    => 'term_id',
			'terms'    => array( $cate_id ),
			'operator' => 'IN',
		),
	),
	//'meta_value' => '',
	//'orderby' => 'meta_value_num',
);

$all_coupons =  wpcoupon_get_cate_coupons( $term_id, 100, 1 );
$cate_title =  get_term($cate_id)->name;
$des = get_queried_object()->description;
$src = get_term_meta($cate_id, $key = '_wpc_cat_image',  $single = false );
$layout = wpcoupon_get_option( 'coupon_cate_layout', 'right-sidebar' );
$available_store = get_available_store($all_coupons);
$available_discount_type = get_available_discount_type($all_coupons);
$coupon_ids_by_type = get_coupon_ids_by_type($all_coupons);
$count =  count_coupon_type_by_cat($cate_id);

?>
<section class="custom-page-header">
     
        <section class="page-header-cover" style="background-image: url(&quot;<?php echo($src[0]); ?>&quot;); 
                        background-repeat: no-repeat;
                        -webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                        background-position: center;
                        ">
                <div class="container">
                    <div class="inner">
                        <div class="inner-content clearfix">
                            <div class="header-content">
                                <h1><?php echo $cate_title;  ?></h1>                            
                            </div>
                            <?php
                                /**
                                * Hooked
                                * @see wpcoupon_breadcrumb() - 15
                                *
                                * @since 1.0.0
                                */
                                do_action( 'wpcoupon_before_container' );
                             ?>
                           
                    </div>
                </div>
            </section>
    <div class="container top-store-taxonomy-cat">
		<div class="clearfix">
			<!-- <?php

			$image_id = get_term_meta( $cate_id, '_wpc_cat_image_id', true );
			$thumb = false;
			if ( $image_id > 0 ) {
				$image= wp_get_attachment_image_src( $image_id, 'medium' );
				if ( $image ) {
					$thumb = '<img src="'.esc_attr( $image[0] ).'" alt=" ">';
				}
			}

			if ( ! $thumb ) {
				$icon = get_term_meta( $cate_id, '_wpc_icon', true );
				if ( trim( $icon ) !== '' ){
					$thumb = '<i class="circular '.esc_attr( $icon ).'"></i>';
				}
			}

			if ( $thumb ) {
			?>
			<div class="header-thumb">
				<div class="ui center aligned icon">
					<?php
					echo apply_filters( '_wpcoupon_cat_thumb', $thumb );
					?>
				</div>
			</div>
			<?php } ?>


			<div class="header-content">
				<?php
				$heading =  wpcoupon_get_option('coupon_cate_heading', esc_html__('Newest %coupon_cate% Coupons', 'wp-coupon') );
				$heading = str_replace( '%coupon_cate%', $cate_title, $heading );
				?>
				<h1><?php echo wp_kses_post( $heading ); ?></h1>
				<?php the_archive_description('<p>', '</p>'); ?>

				<?php
				if ( wpcoupon_get_option( 'coupon_cate_socialshare' ) ) {
				?>
				<div class="entry-share">
					<div class="social-likes social-likes_single" data-single-title="<?php echo esc_attr_e( 'Share', 'wp-coupon' ); ?>">
						<div class="facebook" title="<?php esc_attr_e( 'Share link on Facebook', 'wp-coupon' ); ?>"><?php esc_html_e( 'Facebook', 'wp-coupon' ); ?></div>
						<div class="twitter" title="<?php esc_attr_e( 'Share link on Twitter', 'wp-coupon' ); ?>"><?php esc_html_e( 'Twitter', 'wp-coupon' ) ?></div>
						<div class="plusone" title="<?php esc_attr_e( 'Share link on Google+', 'wp-coupon' ); ?>"><?php esc_html_e( 'Google+', 'wp-coupon' ); ?></div>
						<div class="pinterest" title="<?php esc_attr_e( 'Share image on Pinterest', 'wp-coupon' ); ?>" data-media=""><?php esc_html_e( 'Pinterest', 'wp-coupon' ); ?></div>
					</div>
					<div class="social-likes">
						<div class="facebook" title="<?php esc_attr_e( 'Share link on Facebook', 'wp-coupon' ); ?>"><?php esc_html_e( 'Facebook', 'wp-coupon' ); ?></div>
						<div class="twitter" title="<?php esc_attr_e( 'Share link on Twitter', 'wp-coupon' ); ?>"><?php esc_html_e( 'Twitter', 'wp-coupon' ); ?></div>
						<div class="plusone" title="<?php esc_attr_e( 'Share link on Google+', 'wp-coupon' ); ?>"><?php esc_html_e( 'Google+', 'wp-coupon' ); ?></div>
						<div class="pinterest" title="<?php esc_attr_e( 'Share image on Pinterest', 'wp-coupon' ); ?>" data-media=""><?php esc_html_e( 'Pinterest', 'wp-coupon' ); ?></div>
					</div>
				</div>
				<?php } ?>
				
					

			</div>-->
				
					<div class="inner">
						<div class="inner-content clearfix">
							<div class="header-content">
								<h1>Top Travel Stores</h1>                           
							</div>
						</div>
					</div>
					<div class="coupons-tab-contents">
						<div class="ajax-coupons coupon-tab-content latest-tab">
							<div class="store-listings st-list-coupons">
								<?php
									// get featured stores
									$featured_stores =  wpcoupon_get_featured_stores(6);

									if ( count( $featured_stores ) ) { ?>
									<div class="store-listing-box store-popular-listing interior-category">
										<div class="store-letter-content">
											<div class="col-12 clearfix">
												<?php
												foreach ( $featured_stores as $store ) {
													wpcoupon_setup_store( $store );
												?>
												<div class="col-2 featured-stores fleft">
													<div class="shadow-box">
														<div class="store-thumb">
															<a href="<?php echo get_term_link( $store, 'coupon_store' ) ?>" class="ui image middle aligned  center-div">
																<?php echo wpcoupon_store()->get_thumbnail(); ?>
															</a>
														</div>
														<div class="latest-coupon">
															<?php echo wpcoupon_store()->name; ?>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php } ?>      
							</div>
							<aside id="text-9" class="widget widget_text">			
								<div class="textwidget">
									<div style="text-align:center;padding-bottom:40px">
										<a class="show-all-button" href="">Show All Travel Stores</a> 
									</div>
								</div>
							</aside>
						</div>
					</div>
		</div>
        
    </div>
</section>
<!--
<div id="content-wrap" class="container <?php echo esc_attr( $layout ); ?>">

    <div id="primary" class="content-area">
        <main id="main" class="site-main ajax-coupons" role="main">
            <?php
            $sub_heading =  wpcoupon_get_option( 'coupon_cate_subheading' );
            $sub_heading = str_replace( '%coupon_cate%', $cate_title, $sub_heading );
            ?>
            <section id="store-listings-wrapper" class="st-list-coupons wpb_content_element">
                <?php if ( $sub_heading ) { ?>
                <h2 class="section-heading"><?php echo wp_kses_post( $sub_heading ); ?></h2>
                <?php } ?>
                <?php
                global $wp_rewrite;
                $paged =  wpcoupon_get_paged();
                $args = array(
                    'tax_query' => array(
                        'relation' => 'AND',
                        array(
                            'taxonomy' => 'coupon_category',
                            'field'    => 'term_id',
                            'terms'    => array( $cate_id ),
                            'operator' => 'IN',
                        ),
                    ),
                    //'meta_value' => '',
                    //'orderby' => 'meta_value_num',
                );

                $coupons =  wpcoupon_get_coupons( $args, $paged , $max_pages );
                $current_link = $_SERVER['REQUEST_URI'];

                if (  $coupons )  {
                    foreach ( $coupons as $post ) {
                        wpcoupon_setup_coupon( $post , $current_link );
                        get_template_part( 'loop/loop-coupon-category', 'cat' );
                    }
                }

                ?>
            </section>
            <?php

            if ( 'ajax_loadmore' ==  wpcoupon_get_option( 'coupon_cate_paging', 'ajax_loadmore' ) ) {
                if ($max_pages > $paged) { ?>
                    <div class="load-more wpb_content_element">
                        <a href="<?php next_posts($max_pages); ?>" class="ui button btn btn_primary btn_large" data-next-page="<?php echo($paged + 1); ?>"
                           data-link="<?php echo esc_attr($current_link); ?>" data-cat-id="<?php echo esc_attr( $cate_id ); ?>"
                           data-loading-text="<?php esc_attr_e('Loading...', 'wp-coupon'); ?>"><?php esc_html_e('Load More Coupons', 'wp-coupon'); ?> <i class="arrow circle outline down icon"></i></a>
                    </div>
                <?php }
            } else {
                get_template_part( 'content', 'paging' );
            }

            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

    <!--<div id="secondary" class="widget-area sidebar" role="complementary">
        <?php
        dynamic_sidebar( 'sidebar-coupon-category' );
        ?>
    </div>

    <?php
    $ads = wpcoupon_get_option( 'coupon_cate_ads', '' );
    if ( $ads ) {
        echo '<div class="clear"></div>';
        echo balanceTags( $ads );
    }
    ?>

</div>  -->
<section class="custom-page-header single-store-header category-interior">
    <div class="container">
    	<div class="inner-container">
			<div class="ui grid">
				<div class="border-info-store ui grid hidden-sm hidden-xs">
					<div class="visible-xs four wide tablet column visible-sm" >
						<div class="box shadow-box">
								<div class="box-logo-store">
									<a rel="nofollow" target="_blank" title="<?php esc_html_e( 'Shop ', 'wp-coupon' ); echo wpcoupon_store()->get_display_name(); ?>" href="<?php echo wpcoupon_store()->get_go_store_url(); ?>">
										<?php
											echo wpcoupon_store()->get_thumbnail();
										?>
									</a>
								</div>
								<div class="store-unitlies">
									<ul>
										<li>
											<a href="#"><i class="mail forward icon"></i> SHOP THIS STORE</a>
										</li>
										<li>
											<a href="#"> <i class="empty star icon"></i> SAVE</a>
										</li>
									</ul>
								</div>
							</div>
					</div>
					<?php
						$content = wpcoupon_store()->get_content();
						if($content == '')
						{

						}else{
							?>
							<div class="twelve wide column visible-sm hidden-xs coupon-item " >
								<div class="store-title">
									<h1><?php echo $cate_title;  ?> Promo Codes & Deals</h1>
									<div class="color_black">User Rating</div>
									<div class="rating">
										<span><i class="star icon"></i></span>
										<span><i class="star icon"></i></span>
										<span><i class="star icon"></i></span>
										<span><i class="star icon"></i></span>
										<span><i class="star half empty icon"></i></span>
										<span class="color_black">4.75</span>
									</div>
									<?php
										if(str_word_count($des) > 35){
											$subdecs= wp_trim_words( $des, 35 );
									?>
											<div class="coupon-des">
												<div class="coupon-des-ellip"><?php echo($subdecs) ?>
													<span class="c-actions-span"><a class="more" href="#">+ See More</a></span>
												</div>
												<div class="coupon-des-full"><p><?php echo($des) ?> <a class="more less" href="#">Less</a></p>
												</div>
											</div>
									<?php
										} else{ 
											$subdecs = get_the_content();
									?>
										<div class="coupon-des">
												<div class="coupon-des-ellip">
													<p><?php echo($content) ?> </p>
												</div>
											</div>
									<?php
										} 
									?>
									
								</div>
									
							</div>
							<?php
						}

						?>
					</div>
		     	<div class="four wide tablet four computer column hidden-xs" id="area_left">
		     		
					<!--  Refine by -->
					<div class="box-refine">
						<h2 class="title">
							Refine by
						</h2>
						<h2 class="title_type">Coupon Type</h2>
						<ul class="checklist">
							<li><input type="checkbox" id="i1" class="filter_coupon" value="<?php echo implode(',', $coupon_ids_by_type['code']); ?>"> <label for="i1">Coupon codes (<?php echo absint( $count['code'] ); ?>)</label></li>
							<li><input type="checkbox" id="i2" class="filter_coupon" value="<?php echo implode(',', $coupon_ids_by_type['sale']); ?>"> <label for="i2">Online Sales (<?php echo absint( $count['sale'] ); ?>)</label></li>
							<li><input type="checkbox" id="i3" class="filter_coupon" value="<?php echo implode(',', $coupon_ids_by_type['print']); ?>"> <label for="i3">Printable (<?php echo absint( $count['print'] ); ?>)</label></li>
						</ul>
						<h2 class="title_type">Store</h2>
						<ul class="checklist category-checklist">
							<?php
								$store_count = 1;
								foreach ($available_store as $k => $v) {
									echo '<li class="'. ($store_count > 10 ? 'hide-category' : '') .'"><input type="checkbox" name="category_cb" class="filter_coupon" id="category_cb_'. $k .'" value="'. implode(',', $v['coupon_ids']) .'"> <label for="category_cb_'. $k .'">'. $v['name'] .' ('. $v['count'] .')</label></li>';
									$store_count++;
								}
							?>

							<?php 
								if ((! empty($available_store)) && ($store_count > 10) ) {
									echo '<li class="seemore">+ See More</li>';
								}
							?>
														
						</ul>
						<h2 class="title_type">Discount Type</h2>
						<ul class="checklist">
							<?php 
								foreach ($available_discount_type as $k => $v) {
									echo '<li><input type="checkbox" name="discount_type_cb" class="filter_coupon" id="discount_cb_'. $k .'" value="'. implode(',', $v['coupon_ids']) .'"> <label for="discount_cb_'. $k .'">'. $v['name'] .' ('. $v['count'] .')</label></li>';
								}
							?>
						</ul>				
							<div class="box-submit-2">
								<a href="#" class="btn-submit-coupon"><i class="tags icon"></i> Submit a Coupon</a>
							</div>
							
																		
							<?php dynamic_sidebar('sidebar-store'); ?>
							
							<h2 class="title-store"> Related Categories</h2>
							<ul class="whoview">
								<li><a href="#" >Adorama</a></li>
								<li><a href="#" >Advance Auto Parts</a></li>
								<li><a href="#" >Amazon</a></li>
								<li><a href="#" >Apple Store</a></li>
								<li><a href="#" >AT&T Wireless</a></li>
								<li><a href="#" >BH Photo & Video</a></li>
								<li><a href="#" >Buy Dig</a></li>
								<li><a href="#" >Costco</a></li>
								<li><a href="#" >Dell Home & Office</a></li>
								<li><a href="#" >eBay</a></li>
								<li><a href="#" >Finish Line</a></li>
								<li><a href="#" >Gamestop</a></li>
								<li><a href="#" >Gilt City</a></li>
								<li><a href="#" >Groupon</a></li>
								<li><a href="#" >Home Depot</a></li>
								<li><a href="#" >Jet.com</a></li>
								<li><a href="#" >Jomashop</a></li>								
								<li><a href="#" >Kohl's Living Social</a></li>
								<li><a href="#" class="seemore" >+ See More</a></li>
							</ul>
							<?php
								if($des == '')
								{

								}else{
									?>
									<div class="twelve wide column coupon-item " >
										<div class="store-title">
											<h2 class="title-store"><?php echo $cate_title;  ?> Promo Codes & Deals</h2>
											
											<?php
												if(str_word_count($des) > 35){
													$subdecs= wp_trim_words( $des, 35 );
											?>
													<div class="coupon-des">
														<div class="coupon-des-ellip"><?php echo($subdecs) ?>
															<span class="c-actions-span"><a class="more" href="#">+ See More</a></span>
														</div>
														<div class="coupon-des-full"><p><?php echo($des) ?> <a class="more less" href="#">Less</a></p>
														</div>
													</div>
											<?php
												} else{ 
													$subdecs = get_the_content();
											?>
												<div class="coupon-des">
														<div class="coupon-des-ellip">
															<p><?php echo($content) ?> </p>
														</div>
													</div>
											<?php
												} 
											?>
											
										</div>
											
									</div>
									<?php
								}
							?>
						</div>	
					
<div style="margin-top:30px"></div><script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=ab05f2c9-fa37-4e5b-b316-d12886575f95&storeId=off0ee20-20"></script>
					
					<!--  End -->
		     	</div>
		     	
		     	<div class="twelve wide tablet four computer column" id="area_right">
		     		<div class="hidden">
			     		<div class="store-title">
			     			<h1><?php echo $cate_title;  ?>  Discount Coupons & Deals</h1>
			     			<div class="box-refine-xs">
			     				<a href="#" onClick="return false;" id="refine" class="btn-submit-coupon"><i class="arrow circle outline right icon position-refine"></i> Refine by <i class="arrow circle outline right icon after-refine"></i></a>
			     			</div>
			     			<div class="box-submit-coupon-1">
			     				<a href="#" onClick="return false;" class="btn-submit-coupon"><i class="tags icon"></i> Submit a Coupon</a>
			     			</div>
			     			<div class="clear"></div>
			     		</div>
		     		</div>
		     		<div class="btn_function">
		     			<span id="close"><i class="arrow circle outline left icon"></i>Close</span>
		     		</div>
		     		<?php
		            
					$coupons = wpcoupon_get_cate_coupons( $term_id, $number_active, 1, 'active' );
		            $coupons_unpopular = wpcoupon_get_cate_coupons( $term_id, $number_active, 1, 'unpopular' );
	             	$coupons_expires = wpcoupon_get_cate_coupons( $term_id, $number_active, 1, 'expires' );

	             		              		           
		            ?>
		     		
		     		<div class="deal-loop">
		     			<?php
                            foreach ($coupons as $coupon) {
                                wpcoupon_setup_coupon( $coupon );
                                $post = $coupon->post;
                                setup_postdata($post);
                                get_template_part('loop/loop-coupon-category');
                            }
                         ?>
		     		</div>
					 <!-- Unpopular Coupons -->
		     		<?php 
					 	if (count($coupons_unpopular)) {?>
					 		<div class="box2">
					     		<div class="deal-loop">
					     			<div class="ui grid">
					     				<div class="sixteen wide column title-unpopular-xs">
					     					<h2 class="title2">Unpopular Coupons</h2>
					     					<span>These might not work, but give 'em a try</span>
					     				</div>
					     			</div>
					     			<?php
			                            foreach ($coupons_unpopular as $coupon) {
			                                wpcoupon_setup_coupon( $coupon );
			                                $post = $coupon->post;
			                                setup_postdata($post);
			                                get_template_part('loop/loop-coupon-category');
			                            }
		                         	?>
					     						     			 			     						     			 			     
			     				</div>
		     				</div>
					 <?php } ?>
     				<!-- END -->
     				
     				
     				<!-- Expired Coupons -->
		     		<?php 
					 	if (count($coupons_expires)) {?>
					 		<div class="box3">
					     		<div class="deal-loop">
					     			<div class="ui grid">
					     				<div class="sixteen wide column title-expired-xs">
					     					<h2 class="title2">Expired Coupons</h2>
					     					<span>Some offers may still work beyond their expiration date.</span>
					     				</div>
					     			</div>
					     			<?php
			                            foreach ($coupons_expires as $coupon) {
			                                wpcoupon_setup_coupon( $coupon );
			                                $post = $coupon->post;
			                                setup_postdata($post);
			                                get_template_part('loop/loop-coupon-category');
			                            }
		                         	?>
					     			
			     				</div>
		     				</div>
					 <?php } ?>
     				<!-- END -->
	     		</div>
	     		<?php
								if($des == '')
								{

								}else{
									?>
									<div class="sixteen wide tablet four computer column visible-sm about-store-xs coupon-item hidden hidden-xs">
										<div class="store-title">
											<h1><?php echo $cate_title;  ?></h1>
											<div class="color_black">User Rating</div>
											<div class="rating">
												<span><i class="star icon"></i></span>
												<span><i class="star icon"></i></span>
												<span><i class="star icon"></i></span>
												<span><i class="star icon"></i></span>
												<span><i class="star half empty icon"></i></span>
												<span class="color_black">4.75</span>
											</div>
											<?php
												if(str_word_count($des) > 35){
													$subdecs= wp_trim_words( $des, 35 );
											?>
													<div class="coupon-des">
														<div class="coupon-des-ellip"><?php echo($subdecs) ?>
															<span class="c-actions-span"><a class="more" href="#">+ See More</a></span>
														</div>
														<div class="coupon-des-full"><p><?php echo($des) ?> <a class="more less" href="#">Less</a></p>
														</div>
													</div>
											<?php
												} else{ 
													$subdecs = get_the_content();
											?>
												<div class="coupon-des">
														<div class="coupon-des-ellip">
															<p><?php echo($content) ?> </p>
														</div>
													</div>
											<?php
												} 
											?>
											
										</div>
											
									</div>
									<?php
								}
							?>
	     		
	     		
	     	 	<!--<div class="sixteen wide tablet four computer column visible-sm">
	     	 		<h2 class="title-whoview"> People who viewed Froyo deals also viewed</h2>
							<div class="whoview">
								<div>
									<ul>
										<li><a href="#" >Adorama</a></li>
										<li><a href="#" >Advance Auto Parts</a></li>
										<li><a href="#" >Amazon</a></li>
										<li><a href="#" >Apple Store</a></li>
										<li><a href="#" >AT&T Wireless</a></li>
										<li><a href="#" >BH Photo & Video</a></li>
										<li><a href="#" >Buy Dig</a></li>
										<li><a href="#" >Costco</a></li>
										<li><a href="#" >Dell Home & Office</a></li>
										<li><a href="#" >eBay</a></li>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
										<li><a href="#" >Jet.com</a></li>
										<li><a href="#" >Jomashop</a></li>
										<li><a href="#" >Kohl's Living Social</a></li>
										<li><a href="#" class="seemore" >+ See More</a></li>		
									</ul>
								</div>
								 
							</div>
				</div>	-->		
	     	 	
	     	</div>
        </div>
    </div>
</section>
<script src="/wp-content/themes/wp-coupon/assets/js/customjs.js"></script>
<script>
	jQuery(function ($) {
		$('.box-refine .widget_newsletter .button').html('<i class="arrow right icon"></i>');
		$('#refine').click(
			function () {
				
        });
		
		var click = 0;
		
		$('#close').click(function() {
			var pageW = $(window).width();
			$('#area_left').toggleClass('hidden-sm');
			if(click == 0 )
			{	
				
				var infostoreH = ($('.twelve.wide.column.visible-sm.hidden-xs').height());
				$('#area_left').css("position","relative");
				$('#area_left').animate({ "left": - pageW * 0.25 }, "fast" );
				$('#area_right').css("top",infostoreH + 110);
				$('#area_right').css("position","absolute");
				$('#area_right').animate({ "left": 0 }, "fast" );
				$('#area_right').attr('style','width: 100% !important');
				$('#close').html('<i class="arrow circle outline right icon"></i> Expand');
				click = 1;
			}else {			
				$('#area_left').animate({ "left": 0 }, "fast" );
				$('#area_right').attr('style','width: 75% !important');
				$('#close').html('<i class="arrow circle outline left icon"></i> Close');			
				click =0;
			}					
		});
		$('.box-refine-xs').click(function(){
			var pageW = $(window).width();
                var widthsr =  $('.primary-navigation .st-menu').width();
                $('#area_left').toggleClass("st-menu-mobile");
				$('#area_left').toggleClass("hidden-xs");
                $('#refine').toggleClass("toogle");
                if($( "#refine").hasClass( "toogle" ))
                {
					var infostoreH = ($('.border-info-store.ui.grid').height());
					$('#area_left').attr('style','width:80% !important');
					$('#area_left').css("position","static");
					$('#area_right').css("position","absolute");
					$('#area_right').css("width","100%");
					$('#area_right').css("top",'43px');
                    $('#area_left').animate({ "left": 0 }, "fast" );
                    $('#area_right').animate({ "left": pageW*0.8 },"fast" );
					$('#area_right').css("left",pageW*0.8);
					$('#area_right').css("display","inline-block");
					$('.position-refine').animate({ "margin-left": "-65px" },"fast" );
					$('.position-refine').fadeToggle( "slow", "linear" );
					$('.after-refine').fadeToggle( "slow", "linear" );
					
                }
                else{
                    $('#area_right').animate({ "left": 0 },"fast" );
					$('#area_right').css("position","static");
					$('#area_left').css("position","absolute");
					//$('#area_left').css("top",top + 100);
					$('#area_left').css("left",- pageW*0.8);
					$('.position-refine').css("margin-left", "0"  );
					$('.after-refine').fadeToggle( "slow", "linear" );
					$('.position-refine').fadeToggle( "slow", "linear" );
                }		
		});
									 
	});
	
</script>
<?php
get_footer();
?>
