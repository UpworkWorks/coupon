<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WP Coupon
 */

global $st_option;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta HTTP-EQUIV="Cache-Control" CONTENT="max-age=604800">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
    <style>
        /*------Spookyhalloween deals -----*/
        .header-halloween a.halloween{display:none;}
        /*------Show all halloween saving -----*/
        .content-widgets.frontpage-before-main .show-all-button{display:none;}
        /*------footer newsleter-----*/
        #footer-3{display:none;}
        /*------footer halloween deals-----*/
        #footer-4{display:none;}
        /*------Categories show all store-----*/
        #text-9 .show-all-button{display:none;}
        /*------Author of coupon-----*/
        .deal-loop .items .author{display: none;}
        /*------Submit a coupon button-----*/
        .store-title .box-submit-coupon-1{display: none;}
        /*------Left area in store-----*/
        .hidden.coupon-item{display:none !important;}
        /*------one column on Menu footer------*/
        .site-footer ul.menu {-webkit-column-count: 1;-moz-column-count: 1;column-count: 1;}
        /*------ Hidden category coupon header ------*/
        .top-store-taxonomy-cat,.tax-coupon_category .page-header-cover{display: none;}
        /*------ Hide newsletter ---------*/
        .coupon-modal .coupon-footer { display: none;}
        /*------ Hide left column on interior category------*/
        .category-interior aside#st_newsletter-7{display:none;}
        .category-interior .box-refine .title-store{display:none;}
        .category-interior .box-refine .whoview{display:none;}
        .category-interior .twelve.wide.column.coupon-item{display:none;}
        .category-interior .box-refine .box-submit-2{display:none;}
        /*------hide text in blog------*/
        .featured-post .header-content{display:none;}
    </style>
</head>
<body <?php body_class(); ?>>
    <div id="page" class="hfeed site">
    	<header id="masthead" class="ui page site-header" role="banner">
            <?php do_action('wpcoupon_before_header_top'); ?>
            <div class="primary-header">
                <div class="container">
                     <div class="nav-user-action fright clearfix">
                        <?php

                        if ( class_exists( 'WPCoupon_User' ) ) {
                            WPCoupon_User::nav();
                        }
                        ?>
                    </div> <!-- END .nav_user_action -->
                </div>
                <div class="container">
                    <div class="col-5 fleft logo">
                        <div class="logo_area fleft col-4">
                            <?php if ( wpcoupon_get_option('site_logo', false, 'url') != '' ) { ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                                <img src="<?php echo wpcoupon_get_option('site_logo', false, 'url'); ?>" alt="<?php get_bloginfo( 'name' ) ?>" />
                            </a>
                            <?php } else { ?>
                            <div class="title_area">
                                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                            </div>
                            <?php } ?>
                        </div>
                        <?php
                    $header_icons = wpcoupon_get_option( 'header_icons' );
                    if ( $header_icons ) {
                    ?>
                   <div class="header-halloween fright col-8">
                        <?php
                        foreach( $header_icons as $icon ){
                        ?>
                        <a href="<?php echo ( $icon['url'] ) ? esc_attr( $icon['url'] ) : '#'; ?>" class="halloween">
                            <?php echo esc_html( $icon['title'] ); ?>
                        </a>
                        <?php } ?>

                    </div>
                    <?php } ?>
                        
                    </div>
                    <div class="header_right col-7 fright">
                        <form action="<?php echo home_url( '/' ); ?>" method="get" id="header-search">
                            <div class="header-search-input ui search large action left icon input">
                                <input autocomplete="off" class="prompt" name="s" placeholder="<?php esc_attr_e( 'Search stores for coupons, deals ...', 'wp-coupon' ); ?>" type="text">
                                <input type="hidden" name="search_type" value="store">
                                <i class="search icon"></i>
                                <div class="header-search-submit ui button"><?php esc_html_e( 'Search', 'wp-coupon' ); ?></div>
                                <div class="results"></div>
                            </div>
                            <div class="clear"></div>
                        </form>
                    </div>
                </div>
            </div> <!-- END .header -->

            <?php do_action('wpcoupon_after_header_top'); ?>

            <div class="site-navigation">
                <div class="container">
                    <nav class="primary-navigation clearfix fleft" role="navigation">
                        <a href="#content" class="screen-reader-text skip-link"><?php esc_html_e( 'Skip to content', 'wp-coupon' ); ?></a>
                        <div id="nav-toggle"><i class="content icon"></i></div>
                        <ul class="st-menu">
                            <?php
                                $header_icons = wpcoupon_get_option( 'header_icons' );
                                if ( $header_icons ) {
                                ?>
                                <li class="header-halloween">
                                    <?php
                                    foreach( $header_icons as $icon ){
                                    ?>
                                    <a href="<?php echo ( $icon['url'] ) ? esc_attr( $icon['url'] ) : '#'; ?>" class="halloween">
                                        <?php echo esc_html( $icon['title'] ); ?>
                                    </a>
                                    <?php } ?>

                                </li>
                            <?php } ?>
                            <!--Nav-->
                                <?php
                                if ( class_exists( 'WPCoupon_User' ) ) {
                                   if ( ! function_exists( 'WP_Users' ) ) {
                                        return ;
                                    }

                                    $is_logged_in = is_user_logged_in();
                                    $user =  wp_get_current_user();
                                    $link =  WP_Users()->get_profile_link( $user );
                                    $favorite_link = '#';
                                    $saved_link = '#';

                                    if ( $is_logged_in ){
                                        $class="menu-item-has-children";
                                        $favorite_link =  add_query_arg(  array( 'wpu_action' => 'favorites_stores' ),  $link );
                                        $saved_link =  add_query_arg(  array( 'wpu_action' => 'saved_coupons' ),  $link );
                                    }

                                    // Check if WP_Users Plugin is actived
                                    if ( function_exists( 'WP_Users' ) ) {
                                        ?>
                                       
                                            <li class="navuser">
                                                <a href="<?php echo esc_url( $saved_link ); ?>">
                                                    <i class="empty star icon"></i> <span class="hide-on-tiny"><?php esc_html_e( 'Saved', 'wp-coupon' ); ?></span>
                                                </a>

                                                <div class="menu-box ajax-saved-coupon-box">
                                                    <?php
                                                    if ( $is_logged_in ) {
                                                        WPCoupon_User::recent_saved_coupons_box( $user );
                                                    } else {
                                                        ?>
                                                        <div class="nothing-box stuser-login-btn">
                                                            <div class="thumb">
                                                                <i class="frown icon"></i>
                                                            </div>
                                                            <p><?php esc_html_e( 'Please login to see your saved coupons', 'wp-coupon' ); ?></p>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    </div>
                                            </li>
                                            <li class="navuser">
                                                <a href="<?php echo esc_url( $favorite_link );  ?>">
                                                    <i class="empty heart icon"></i> <span class="hide-tiny-screen"><?php esc_html_e( 'Favorites', 'wp-coupon' ); ?></span>
                                                </a>

                                                <div class="menu-box ajax-favorite-stores-box">
                                                    <?php
                                                    if ( $is_logged_in ) {
                                                        WPCoupon_User::recent_favorite_stores_box( $user );
                                                    } else {
                                                        ?>
                                                        <div class="nothing-box stuser-login-btn">
                                                            <div class="thumb">
                                                                <i class="frown icon"></i>
                                                            </div>
                                                            <p><?php esc_html_e( 'Please login to see your favorite stores', 'wp-coupon' ); ?></p>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <li class="navuser <?php $class ?>">
                                                <a data-is-logged="<?php echo is_user_logged_in() ? 'true' : 'false'; ?>" class="wpu-login-btn" href="<?php echo WP_Users()->get_profile_link(); ?>"><i class="power icon"></i><?php echo ( $is_logged_in ) ?  esc_html__( 'Dashboard', 'wp-coupon' ) :  esc_html__( 'Login', 'wp-coupon' ); ?></a>
                                                <?php if ( $is_logged_in ) { ?>
                                                <ul class="sub-menu">
                                                    <li><a href="<?php echo esc_url( $link ); ?>"><?php esc_html_e( 'Dashboard', 'wp-coupon' ); ?></a></li>
                                                    <li><a href="<?php echo WP_Users()->get_edit_profile_link( $user ); ?>"><?php esc_html_e( 'Account Settings', 'wp-coupon' ); ?></a></li>
                                                    <li><a href="<?php echo wp_logout_url(); ?>"><?php esc_html_e( 'Sign Out', 'wp-coupon' ); ?></a></li>
                                                </ul>
                                                <?php 
                                                }
                                                 ?>
                                            </li>
                                        <?php
                                }
                            ?>
                            <?php } ?>

                            <!--end Nav-->
                           <?php wp_nav_menu( array('theme_location' => 'primary', 'container' => '', 'items_wrap' => '%3$s' ) ); ?>
                            <li  class="menu-item menu-item-type-post_type menu-item-object-page navuser">
                                <a href="/about-us/">About us</a>
                            </li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page navuser">
                                <a href="/contact-us/">Contact us</a>
                            </li>
                        </ul>
                    </nav> <!-- END .primary-navigation -->
                    <div class="logo_area fleft col-5">
                        <?php if ( wpcoupon_get_option('site_logo', false, 'url') != '' ) { ?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                            <img src="<?php echo wpcoupon_get_option('site_logo', false, 'url'); ?>" alt="<?php get_bloginfo( 'name' ) ?>" />
                        </a>
                        <?php } else { ?>
                        <div class="title_area">
                            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                            <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="logomenu fright">
                      <i class="search icon"></i>
                    </div>
                    
                </div> 
                <!-- END .container -->
            </div> <!-- END #primary-navigation -->
           <script type='text/javascript'>//<![CDATA[
                jQuery(function($){
                        // get vars
                        var labelEl = $(".logomenu i.search.icon");
                        var $el = $('.site-navigation');
                        var bottom = $el.position().top + $el.outerHeight(true);
                        var paddT = $('.site-navigation .logo_area').outerHeight() - $('.site-navigation .logo_area').height();
                        $('.site-navigation .logo_area').height($el.height() - paddT);
                        // register clicks and toggle classes
                        labelEl.on("click",function(){
                            $( this ).toggleClass( "active" );
                            if ($( this ).hasClass( "active" )) {
                                $(".primary-header").css("top",bottom);
                            } else {
                                $(".primary-header").css("top","-165px");
                            }
                        });
                         $(window).bind("resize", function(){
                            var $el = $('.site-navigation');
                            var bottom = $el.position().top + $el.outerHeight(true);
                            var paddT = $('.site-navigation .logo_area').outerHeight() - $('.site-navigation .logo_area').height();
                        });
                    });
                
            </script>
    	</header><!-- END #masthead -->
        <div id="content" class="site-content">