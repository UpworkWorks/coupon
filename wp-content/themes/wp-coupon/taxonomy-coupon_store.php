<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP Coupon
 */
get_header();

$term = get_queried_object();
$all_coupons = wpcoupon_get_store_coupons( $term->term_id, 1000, 1 );
$available_categories = get_available_categories($all_coupons);
$available_discount_type = get_available_discount_type($all_coupons);
$coupon_ids_by_type = get_coupon_ids_by_type($all_coupons);
wpcoupon_setup_store( $term  );
$current_link =  get_permalink( $term );
$store_name = wpcoupon_store()->name;
$layout = wpcoupon_get_option( 'store_layout', 'left-sidebar' );
$count =  wpcoupon_store()->count_coupon();

?>
<section class="custom-page-header single-store-header">
    <div class="container">
    	<div class="inner-container">
    		<div class="ui grid">
				<div class="border-info-store ui grid">
	    			<div class="visible-xs four wide tablet column visible-sm" >
	    				<div class="box shadow-box">
				     			<div class="box-logo-store center-div">
						     		<a rel="nofollow" target="_blank" title="<?php esc_html_e( 'Shop ', 'wp-coupon' ); echo wpcoupon_store()->get_display_name(); ?>" href="<?php echo wpcoupon_store()->get_go_store_url(); ?>">
										<?php
											echo wpcoupon_store()->get_thumbnail();
										?>
									</a>
								</div>
								<div class="store-unitlies">
									<ul>
										<li>
											<a title="<?php esc_html_e( 'Shop ', 'wp-coupon' ); echo wpcoupon_store()->get_display_name(); ?>" href="<?php echo wpcoupon_store()->get_go_store_url(); ?>"><i class="mail forward icon"></i> SHOP THIS STORE</a>
										</li>
										<li>
											<a href="#"> <i class="empty star icon"></i> SAVE</a>
										</li>
									</ul>
								</div>
							</div>
	    			</div>
					<?php
						$content = wpcoupon_store()->get_content();
						if($content == '')
						{

						}else{
							?>
							<div class="twelve wide column visible-sm hidden-xs coupon-item " >
								<div class="store-title">
									<h1><?php echo $store_name; ?> Promo Codes & Deals</h1>
									<div class="color_black">User Rating</div>
									<div class="rating">
										<span><i class="star icon"></i></span>
										<span><i class="star icon"></i></span>
										<span><i class="star icon"></i></span>
										<span><i class="star icon"></i></span>
										<span><i class="star half empty icon"></i></span>
										<span class="color_black">4.75</span>
									</div>
									<?php
										if(str_word_count($content) > 35){
											$subdecs= wp_trim_words( $content, 35 );
									?>
											<div class="coupon-des">
												<div class="coupon-des-ellip"><?php /* echo($subdecs) */ ?>
													<span class="c-actions-span"><a class="more" href="#">+ See More</a></span>
												</div>
												<div class="coupon-des-full"><p><?php echo($content) ?> <a class="more less" href="#">Less</a></p>
												</div>
											</div>
									<?php
										} else{ 
											$subdecs = get_the_content();
									?>
										<div class="coupon-des">
												<div class="coupon-des-ellip">
													<p><?php /* echo($content) */ ?> </p>
												</div>
											</div>
									<?php
										} 
									?>
									
								</div>
									
							</div>
							<?php
						}

					 ?>
	    			
    			 </div>
		     	<div class="four wide tablet four computer column hidden-xs w-25" id="area_left">
		     		<div class="hidden hidden-xs">
			     		<div class="box shadow-box">
			     			<div class="box-logo-store center-div">
					     		<a rel="nofollow" target="_blank" title="<?php esc_html_e( 'Shop ', 'wp-coupon' ); echo wpcoupon_store()->get_display_name(); ?>" href="<?php echo wpcoupon_store()->get_go_store_url(); ?>">
									<?php
										echo wpcoupon_store()->get_thumbnail();
									?>
								</a>
							</div>
							<div class="store-unitlies">
								<ul>
									<li>
										<a href="<?php echo esc_attr( wpcoupon_store()->get_go_store_url()) ?>"><i class="mail forward icon"></i> SHOP THIS STORE</a>
									</li>
									<li>
										<a class="add-favorite" data-id="<?php echo wpcoupon_store()->term_id; ?>" href="#"> <i class="empty star icon"></i> SAVE</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					
					
					<!--  Refine by -->
					<div class="box-refine">
						<h2 class="title">
							Refine by
						</h2>
						
						<h2 class="title_type">Coupon Type</h2>
						<ul class="checklist">
							<li><input type="checkbox" id="i1" class="filter_coupon" value="<?php echo implode(',', $coupon_ids_by_type['code']); ?>"> <label for="i1">Coupon codes (<?php echo absint( $count['code'] ); ?>)</label></li>
							<li><input type="checkbox" id="i2" class="filter_coupon" value="<?php echo implode(',', $coupon_ids_by_type['sale']); ?>"> <label for="i2">Online Sales (<?php echo absint( $count['sale'] ); ?>)</label></li>
							<li><input type="checkbox" id="i3" class="filter_coupon" value="<?php echo implode(',', $coupon_ids_by_type['print']); ?>"> <label for="i3">Printable (<?php echo absint( $count['print'] ); ?>)</label></li>
						</ul>
						
						<h2 class="title_type">Category</h2>
						<ul class="checklist category-checklist">
							<?php
								$category_count = 1;
								foreach ($available_categories as $k => $v) {
									echo '<li class="'. ($category_count > 10 ? 'hide-category' : '') .'"><input type="checkbox" name="category_cb" class="filter_coupon" id="category_cb_'. $k .'" value="'. implode(',', $v['coupon_ids']) .'"> <label for="category_cb_'. $k .'">'. $v['name'] .' ('. $v['count'] .')</label></li>';
									$category_count++;
								}
							?>

							<?php 
								if ((! empty($available_categories)) && ($category_count > 10) ) {
									echo '<li class="seemore">+ See More</li>';
								}
							?>
														
						</ul>
						
						<h2 class="title_type">Discount Type</h2>
						<ul class="checklist">
							<?php 
								foreach ($available_discount_type as $k => $v) {
									echo '<li><input type="checkbox" name="discount_type_cb" class="filter_coupon" id="discount_cb_'. $k .'" value="'. implode(',', $v['coupon_ids']) .'"> <label for="discount_cb_'. $k .'">'. $v['name'] .' ('. $v['count'] .')</label></li>';
								}
							?>
						</ul>
						
						<div class="hidden coupon-item ">	
							<div class="about-store">					
							<?php
								$content = wpcoupon_store()->get_content();
								if($content == '')
								{

								}else{
									?>
										<h2 class="title-store">About <?php echo $store_name; ?></h2>
										<div class="color_black">User Rating</div>
										<div class="rating">
											<span><i class="star icon"></i></span>
											<span><i class="star icon"></i></span>
											<span><i class="star icon"></i></span>
											<span><i class="star icon"></i></span>
											<span><i class="star half empty icon"></i></span>
											<span class="color_black">4.75</span>
										</div>
										<?php
											if(str_word_count($content) > 15){
												$subdecs= wp_trim_words( $content, 15 );
										?>
												<div class="coupon-des">
													<div class="coupon-des-ellip"><?php echo($subdecs) ?>
														<span class="c-actions-span"><a class="more" href="#">+ See More</a></span>
													</div>
													<div class="coupon-des-full"><p><?php echo($content) ?>
														<span class="c-actions-span"> <a class="more less" href="#">Less</a></span></p>
													</div>
													<div class="clear"></div>
												</div>
										<?php
											} else{ 
												$subdecs = get_the_content();
										?>
												<div class="coupon-des">
													<div class="coupon-des-ellip">
														<p><?php echo($content) ?> </p>
													</div>
													<div class="clear"></div>
												</div>
										<?php
											} 
										?>
										
									<?php
								}

							?>
							
							</div>
							<div class="box-submit-2">
							<a href="#" class="btn-submit-coupon"><i class="tags icon"></i> Submit a Coupon</a>
							</div>
							
																		
							<?php dynamic_sidebar('sidebar-store'); ?>
							
							<h2 class="title-store"> People who viewed <?php echo $store_name; ?> deals also viewed</h2>
							<ul class="whoview">
								<li><a href="#" >Adorama</a></li>
								<li><a href="#" >Advance Auto Parts</a></li>
								<li><a href="#" >Amazon</a></li>
								<li><a href="#" >Apple Store</a></li>
								<li><a href="#" >AT&T Wireless</a></li>
								<li><a href="#" >BH Photo & Video</a></li>
								<li><a href="#" >Buy Dig</a></li>
								<li><a href="#" >Costco</a></li>
								<li><a href="#" >Dell Home & Office</a></li>
								<li><a href="#" >eBay</a></li>
								<li><a href="#" >Finish Line</a></li>
								<li><a href="#" >Gamestop</a></li>
								<li><a href="#" >Gilt City</a></li>
								<li><a href="#" >Groupon</a></li>
								<li><a href="#" >Home Depot</a></li>
								<li><a href="#" >Jet.com</a></li>
								<li><a href="#" >Jomashop</a></li>								
								<li><a href="#" >Kohl's Living Social</a></li>
								<li><a href="#" class="seemore" >+ See More</a></li>
							</ul>
						</div>	
					</div>
					
<div style="margin-top:30px"></div><script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=ab05f2c9-fa37-4e5b-b316-d12886575f95&storeId=off0ee20-20"></script>
					
					<!--  End -->
		     	</div>
		     	
		     	<div class="twelve wide tablet four computer column w-75" id="area_right">
		     		<div class="hidden">
			     		<div class="store-title">
			     			<h1><?php echo $store_name; ?> Promo Codes & Deals</h1>
			     			<div class="box-refine-xs">
			     				<a href="#" onClick="return false;" id="refine" class="btn-submit-coupon"><i class="arrow circle outline right icon position-refine"></i> Refine by <i class="arrow circle outline right icon after-refine"></i></a>
			     			</div>
			     			<div class="box-submit-coupon-1">
			     				<a href="#" onClick="return false;" class="btn-submit-coupon"><i class="tags icon"></i> Submit a Coupon</a>
			     			</div>
			     			<div class="clear"></div>
			     		</div>
		     		</div>
		     		<div class="btn_function">
		     			<a id="close"><i class="arrow circle outline left icon"></i>Close</a>
		     		</div>
		     		<?php
		            /**
		            * Hooked
		            *
		            * @see: wpcoupon_store_coupons_filter -  15
		            * @see wpcoupon_store_coupons_filter
		            * @since 1.0.0
		            */
		             
		
		             /**
		             * get coupons of this store
		             */
		            global $wp_query;
		            $coupons =  $wp_query->posts;
		            $term_id =  get_queried_object_id();		            
		            $number_active = 1000;		          		            
		            $coupons = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'active' );
		            $coupons_unpopular = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'unpopular' );
	             	$coupons_expires = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'expires' );
	             		              		           
		            ?>
		     		
		     		<div class="deal-loop">
		     			<?php
                            foreach ($coupons as $coupon) {
                                wpcoupon_setup_coupon( $coupon );
                                $post = $coupon->post;
                                setup_postdata($post);
                                get_template_part('loop/loop-coupon-custom');
                            }
                         ?>
		     		 
		     		</div>
		     		<!-- Unpopular Coupons -->
		     		<?php 
		     			if (count($coupons_unpopular)) {?>
		     				<div class="box2">
					     		<div class="deal-loop">
					     			<div class="ui grid">
					     				<div class="sixteen wide column title-unpopular-xs">
					     					<h2 class="title2">Unpopular Coupons</h2>
					     					<span>These might not work, but give 'em a try</span>
					     				</div>
					     			</div>
					     			<?php
			                            foreach ($coupons_unpopular as $coupon) {
			                                wpcoupon_setup_coupon( $coupon );
			                                $post = $coupon->post;
			                                setup_postdata($post);
			                                get_template_part('loop/loop-coupon-custom');
			                            }
		                         	?>
					     						     			 			     						     			 			     
			     				</div>
		     				</div>
		     		<?php } ?>
     				<!-- END -->
     				
     				
     				<!-- Expired Coupons -->
		     		<?php 
		     			if (count($coupons_expires)) {?>
		     				<div class="box3">
					     		<div class="deal-loop">
					     			<div class="ui grid">
					     				<div class="sixteen wide column title-expired-xs">
					     					<h2 class="title2">Expired Coupons</h2>
					     					<span>Some offers may still work beyond their expiration date.</span>
					     				</div>
					     			</div>
					     			<?php
			                            foreach ($coupons_expires as $coupon) {
			                                wpcoupon_setup_coupon( $coupon );
			                                $post = $coupon->post;
			                                setup_postdata($post);
			                                get_template_part('loop/loop-coupon-custom');
			                            }
		                         	?>
					     			
			     				</div>
		     				</div>
		     		<?php } ?>
     				<!-- END -->
		     		
	     		</div>
	     		
	     		<div class="sixteen wide tablet four computer column visible-sm about-store-xs hidden hidden-xs">
	     			<div class="store-title">
			     			<h1><?php echo wpcoupon_store()->get_single_store_name(); ?></h1>
			     			<div class="color_black">User Rating</div>
							<div class="rating">
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star half empty icon"></i></span>
								<span class="color_black">4.75</span>
							</div>
							<span class="">
								<?php echo wpcoupon_store()->get_content(); ?>
							</span>
							<a href="#" class="seemore">+ See More</a>
	     			</div>
	     		</div>
	     		
	     	 	<div class="sixteen wide tablet four computer column visible-sm hidden hidden-sm">
	     	 		<h2 class="title-whoview"> People who viewed Froyo deals also viewed</h2>
							<div class="whoview">
								<div>
									<ul>
										<li><a href="#" >Adorama</a></li>
										<li><a href="#" >Advance Auto Parts</a></li>
										<li><a href="#" >Amazon</a></li>
										<li><a href="#" >Apple Store</a></li>
										<li><a href="#" >AT&T Wireless</a></li>
										<li><a href="#" >BH Photo & Video</a></li>
										<li><a href="#" >Buy Dig</a></li>
										<li><a href="#" >Costco</a></li>
										<li><a href="#" >Dell Home & Office</a></li>
										<li><a href="#" >eBay</a></li>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
										<li><a href="#" >Jet.com</a></li>
										<li><a href="#" >Jomashop</a></li>
										<li><a href="#" >Kohl's Living Social</a></li>
										<li><a href="#" class="seemore" >+ See More</a></li>		
									</ul>
								</div>
								 
							</div>
				</div>			
	     	 	
	     	</div>
        </div>
    </div>
</section>
<!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="/wp-content/themes/wp-coupon/assets/js/customjs.js"></script>
<script>
	jQuery(function ($) {
		$('.box-refine .widget_newsletter .button').html('<i class="arrow right icon"></i>');
		$('#refine').click(
			function () {
				
        });
		
		var click = 0;
		
		$('#close').click(function() {
			var pageW = $(window).width();
			
			if(click == 0 )
			{	
				var infostoreH = ($('.border-info-store.ui.grid').height());
				$('#area_left').switchClass( "w-25", "w-0", 1000, "easeInOutQuad" );
				$('#area_left').css('position','absolute');
				$('#area_left').css('z-index','-1');
				$('#area_right').switchClass( "w-75", "w-100", 1000, "easeInOutQuad" );
				$('#close').html('<i class="arrow circle outline right icon"></i> Expand');
				click = 1;
				
			}else {		
				$('#area_left').switchClass( "w-0", "w-25", 1000, "easeInOutQuad" );
				$('#area_right').switchClass( "w-100", "w-75", 1000, "easeInOutQuad" );
				$('#area_left').css('position','relative');
				$('#area_left').css('z-index','0');
				$('#close').html('<i class="arrow circle outline left icon"></i> Close');			
				click =0;
			}					
		});
		$('.box-refine-xs').click(function(){
			var pageW = $(window).width();
                var widthsr =  $('.primary-navigation .st-menu').width();
				var top = $('.border-info-store.ui.grid .visible-xs.four.wide.tablet.column.visible-sm').offset().top +  $('#masthead').height() ;
                $('#area_left').toggleClass("st-menu-mobile");
				$('#area_left').removeClass("hidden-xs");
                $('#refine').toggleClass("toogle");
                if($( "#refine").hasClass( "toogle" ))
                {
					var infostoreH = ($('.border-info-store.ui.grid').height());
					$('#area_left').attr('style','width:80% !important');
					$('#area_left').css("position","absolute");
					$('#area_left').css("top",infostoreH + 30);
                    $('#area_left').animate({ "left": 0 }, "fast" );
                    $('#area_right').animate({ "left": pageW*0.8 },"fast" );
					$('#area_right').css("left",pageW*0.8);
					$('#area_right').css("display","inline-block");
					$('.position-refine').animate({ "margin-left": "-75px" },"fast" );
					$('.position-refine').fadeToggle( "slow", "linear" );
					$('.after-refine').fadeToggle( "slow", "linear" );
					
                }
                else{
                    $('#area_right').animate({ "left": 0 },"fast" );
					$('.position-refine').animate({ "margin-left": "0" },"fast" );
					$('.after-refine').fadeToggle( "slow", "linear" );
					$('.position-refine').fadeToggle( "slow", "linear" );
					$('#area_left').animate({ "left": 0 },"fast" );
					
                }		
		});
	});
	
</script>

<?php get_footer(); ?>
