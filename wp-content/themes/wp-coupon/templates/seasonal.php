<?php
/**
 * Template Name: Interior Seasonal
 *
 * Display Store by alphabet
 *
 * @package ST-Coupon
 * @since 1.0.
 * Get category by slug "interior-seasonal"
 */
  $category = get_term_by( 'slug', 'interior-seasonal', 'coupon_category' );
  $cate_id = $category->term_id;
  $term_cat = get_term( $cate_id, $taxonomy );
  $cate_title = $term_cat->name;
get_header();
/**
 * Hooks wpcoupon_after_header
 *
 * @see wpcoupon_page_header();
 *
 */
 do_action( 'wpcoupon_cover_seasonal' );


?>
<section class="hero">
    <div class="container">
        <div class="inner hero-background box shadow-box">
            <div class="inner-content clearfix">
                <div class="coupons-tab-contents">
                    <div class="ajax-coupons coupon-tab-content latest-tab">
                        <div class="store-listings st-list-coupons"> 
                        <?php 
                            $count=0;
							$remain = 0;
                            $seasonal = featuredPost(4,'seasonal');
							$totalpost =  count($seasonal);
							if($totalpost < 4)
							{
								$remain = 4 - $totalpost;
								$post_new = featuredPost($remain+4,'');
								$post_display = super_unique(array_merge($seasonal,$post_new));
							}else{
								$post_display = $seasonal;
							}
							
                            foreach ( $post_display as $post ) :
                            setup_postdata( $post ); 
                            $count = $count + 1 ;
                            $post_id = get_the_ID();
                        ?>
							<div class="featured-interior fleft featured<?php echo $count ?>">         
								<div class="box-interior">                 
									<div class="store-thumb">
										<a href="<?php the_permalink(); ?>" class="ui image middle aligned  center-div" style="background-image:url(<?php echo the_post_thumbnail_url($post_id, array( 620, 300)); ?>); ">
										</a>
									</div>
									<div class="latest-coupon">
										<a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>			
										<div class="botton">
											<a class="show-all-button" href="">Shop Macksio CTA</a>
										</div>										
									</div>
									
								</div>
							</div>
                        <?php 
							if($count >= 4)
							{
								break;
							}
							endforeach; 
                            wp_reset_postdata();
                        ?> 
                        <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</section>

<section class="custom-page-header single-store-header category-interior">
    <div class="container">
    	<div class="inner-container">
    		<div class="ui grid">
    			 
	    			<div class="visible-xs four wide tablet column visible-sm" >
	    				<div class="box shadow-box">
				     			<div class="box-logo-store">
						     		<a rel="nofollow" target="_blank" title="<?php esc_html_e( 'Shop ', 'wp-coupon' ); echo wpcoupon_store()->get_display_name(); ?>" href="<?php echo wpcoupon_store()->get_go_store_url(); ?>">
										<?php
											echo wpcoupon_store()->get_thumbnail();
										?>
									</a>
								</div>
								<div class="store-unitlies">
									<ul>
										<li>
											<a href="#"><i class="mail forward icon"></i> SHOP THIS STORE</a>
										</li>
										<li>
											<a href="#"> <i class="empty star icon"></i> SAVE</a>
										</li>
									</ul>
								</div>
							</div>
	    			</div>
	    			<div class="twelve wide column visible-sm hidden-xs" >
	    				 <div class="store-title">
			     			<h1><?php echo $cate_title;  ?></h1>
			     			<!--<div class="color_black">User Rating</div>
							<div class="rating">
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star half empty icon"></i></span>
								<span class="color_black">4.75</span>
							</div>-->
							<span>
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. </p>
	<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
							</span>
							<a href="#" class="seemore">+ See More</a>
		     			</div>
			     			
	    			</div>
    			 
    		 
		     	<div class="four wide tablet four computer column hidden-xs" id="area_left">
		     		
					<!--  Refine by -->
					<div class="box-refine">
						<h2 class="title">
							Refine by
						</h2>
						
						<h2 class="title_type">Coupon Type</h2>
						<ul class="checklist">
							<li><input type="checkbox" id="i1"> <label for="i1">Coupon codes (21)</label></li>
							<li><input type="checkbox" id="i2"> <label for="i2">Online Sales (24)</label></li>
							<li><input type="checkbox" id="i3"> <label for="i3">In-Store Coupongs (12)</label></li>
							<li><input type="checkbox" id="i4"> <label for="i4">Gift cards (1)</label></li>
							<li><input type="checkbox" id="i5"> <label for="i5">Only At Offer.Me(1)</label></li>
						</ul>
						
						<h2 class="title_type">Store</h2>
						<ul class="checklist">
							<li><input type="checkbox" id="i6"> <label for="i6">Electronis (25)</label></li>
							<li><input type="checkbox" id="i7"> <label for="i7">Home And Garden (11)</label></li>
							<li><input type="checkbox" id="i8"> <label for="i8">Mutilcategory (3)</label></li>
							<li><input type="checkbox" id="i9"> <label for="i9">Office Supplies (3)</label></li>
							<li><input type="checkbox" id="i10"> <label for="i10">Other (2)</label></li>
							<li><input type="checkbox" id="i11"> <label for="i11">Health (2)</label></li>
							<li><input type="checkbox" id="i12"> <label for="i12">Books, Music And Movies (1)</label></li>
							<li><input type="checkbox" id="i13"> <label for="i13">Clothing & Accessories (1)</label></li>
							<li><input type="checkbox" id="i14"> <label for="i14">Food (1)</label></li>
							<li><input type="checkbox" id="i15"> <label for="i15">Toys & Games (1)</label></li>
							<li class="seemore">+ See More</li>							
						</ul>
						
						<h2 class="title_type">Discount Type</h2>
						<ul class="checklist">
							<li><input type="checkbox" id="c1"> <label for="c1">Free Shipping (4)</label></li>
							<li><input type="checkbox" id="c2"> <label for="c2">Free Gift (2)</label></li>
							<li><input type="checkbox" id="c3"> <label for="c3">$ Off (13)</label></li>
							<li><input type="checkbox" id="c4"> <label for="c4">% Off (28)</label></li>
							<li><input type="checkbox" id="c5"> <label for="c5">Other (6)</label></li>
						</ul>
						<div class="box-submit-2">
							<a href="#" class="btn-submit-coupon"><i class="tags icon"></i> Submit a Coupon</a>
							</div>																		
							<?php dynamic_sidebar('sidebar-store'); ?>
							<h2 class="title-store"> Related Categories</h2>
							<ul class="whoview">
								<li><a href="#" >Adorama</a></li>
								<li><a href="#" >Advance Auto Parts</a></li>
								<li><a href="#" >Amazon</a></li>
								<li><a href="#" >Apple Store</a></li>
								<li><a href="#" >AT&T Wireless</a></li>
								<li><a href="#" >BH Photo & Video</a></li>
								<li><a href="#" >Buy Dig</a></li>
								<li><a href="#" >Costco</a></li>
								<li><a href="#" >Dell Home & Office</a></li>
								<li><a href="#" >eBay</a></li>
								<li><a href="#" >Finish Line</a></li>
								<li><a href="#" >Gamestop</a></li>
								<li><a href="#" >Gilt City</a></li>
								<li><a href="#" >Groupon</a></li>
								<li><a href="#" >Home Depot</a></li>
								<li><a href="#" >Jet.com</a></li>
								<li><a href="#" >Jomashop</a></li>								
								<li><a href="#" >Kohl's Living Social</a></li>
								<li><a href="#" class="seemore" >+ See More</a></li>
							</ul>
						<div class="hidden">						
							<h2 class="title-store"><?php echo $cate_title;  ?></h2>
							<!--<div class="color_black">User Rating</div>
							<div class="rating">
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star half empty icon"></i></span>
								<span>4.75</span>
							</div>-->
							<span>
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. </p>
	<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
							</span>
							<!--<span><a href="#" class="seemore">+  See More</a></span>-->
						</div>	
					</div>
					
					
					<!--  End -->
		     	</div>
		     	
		     	<div class="twelve wide tablet four computer column" id="area_right">
		     		<div class="hidden">
			     		<div class="store-title">
			     			<h1><?php echo $cate_title;  ?></h1>
			     			<div class="box-refine-xs">
			     				<a href="#" class="btn-submit-coupon">Refine by <i class="arrow circle outline right icon"></i></a>
			     			</div>
			     			<div class="box-submit-coupon-1">
			     				<a href="#" class="btn-submit-coupon"><i class="tags icon"></i> Submit a Coupon</a>
			     			</div>
			     			<div class="clear"></div>
			     		</div>
		     		</div>
		     		<div class="btn_function">
		     			<span id="close"><i class="arrow circle outline left icon"></i>Close</span>
		     		</div>
		     		<?php
		            /**
		            * Hooked
		            *
		            * @see: wpcoupon_store_coupons_filter -  15
		            * @see wpcoupon_store_coupons_filter
		            * @since 1.0.0
		            */
		             
		
		             /**
		             * get coupons of this store
		             */
		            global $wp_query;
		            $coupons =  $wp_query->posts;
		            $term_id =  get_queried_object_id();		            
		            $number_active = 100;		          
		          //  $coupons = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'active' );
		         //   $coupons_unpopular = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'unpopular' );
	             //	$coupons_expires = wpcoupon_get_store_coupons( $term_id, $number_active, 1, 'expires' );
                    global $wp_rewrite;
                    $paged =  wpcoupon_get_paged();
                    $args = array(
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'coupon_category',
                                'field'    => 'term_id',
                                'terms'    => array( $cate_id ),
                                'operator' => 'IN',
                            ),
                        ),
                        //'meta_value' => '',
                        //'orderby' => 'meta_value_num',
                    );
					
                    $coupons =  wpcoupon_get_coupons( $args, $paged , $max_pages );	              		           
		            ?>
		     		
		     		<div class="deal-loop">
		     			<?php
                            foreach ($coupons as $coupon) {
                                wpcoupon_setup_coupon( $coupon );
                                $post = $coupon->post;
                                setup_postdata($post);
                                get_template_part('loop/loop-coupon-custom');
                            }
                         ?>
		     		 
		     		</div>
		     		 
     				
     			 
	     		</div>
	     		
	     		<div class="sixteen wide tablet four computer column visible-sm about-store-xs">
	     			<div class="store-title">
			     			<h1><?php echo wpcoupon_store()->get_single_store_name(); ?></h1>
			     			<div class="color_black">User Rating</div>
							<div class="rating">
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star icon"></i></span>
								<span><i class="star half empty icon"></i></span>
								<span class="color_black">4.75</span>
							</div>
							<span class="">
								Sed ut perspic iatis unde omnis iste natus error sit volupta tem accusa ntium dolore mque lauda ntium, totam rem aperiam, eaque ipsa quae ab illo inventore verit atis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam volupta tem quia voluptas sit asper natur aut odit aut fugit, sed quia consequ untur magni dolores.
							</span>
							<a href="#" class="seemore">+ See More</a>
	     			</div>
	     		</div>
	     		
	     	 	<div class="sixteen wide tablet four computer column visible-sm">
	     	 		<h2 class="title-whoview"> People who viewed Froyo deals also viewed</h2>
							<div class="whoview">
								<div>
									<ul>
										<li><a href="#" >Adorama</a></li>
										<li><a href="#" >Advance Auto Parts</a></li>
										<li><a href="#" >Amazon</a></li>
										<li><a href="#" >Apple Store</a></li>
										<li><a href="#" >AT&T Wireless</a></li>
									</ul>
								</div>							
								<div>
									<ul>
										<li><a href="#" >BH Photo & Video</a></li>
										<li><a href="#" >Buy Dig</a></li>
										<li><a href="#" >Costco</a></li>
										<li><a href="#" >Dell Home & Office</a></li>
										<li><a href="#" >eBay</a></li>
									</ul>								
								</div>
								<div>
									<ul>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
									</ul>
								</div>
								<div>
									<ul>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
									</ul>
								</div>
								<div>
									<ul>
										<li><a href="#" >Finish Line</a></li>
										<li><a href="#" >Gamestop</a></li>
										<li><a href="#" >Gilt City</a></li>
										<li><a href="#" >Groupon</a></li>
										<li><a href="#" >Home Depot</a></li>
									</ul>
								</div>
								<div>
									<ul>
										<li><a href="#" >Jet.com</a></li>
										<li><a href="#" >Jomashop</a></li>
										<li><a href="#" >Kohl's Living Social</a></li>
										<li><a href="#" class="seemore" >+ See More</a></li>		
									</ul>
								</div>
								 
							</div>
				</div>			
	     	 	
	     	</div>
        </div>
    </div>
</section>
<script>
	jQuery(function ($) {
		$('.box-refine .widget_newsletter .button').html('<i class="arrow right icon"></i>');
		var click = 0;
		$('#close').click(function() {
			if(click == 0 )
			{		
 				$('#area_left').hide();
				$('#area_right').attr('style','width: 100% !important');
				$('#close').html('<i class="arrow circle outline right icon"></i> Expand');
				click = 1;
			}else {			
				var left = 0;
				for(var i=100; i>=75; i--) {
					$('#area_right').attr('style','width: '+i+'% !important');										
				}
				setTimeout(function(){ $('#area_left').show(); }, 1000);
				$('#close').html('<i class="arrow circle outline left icon"></i> Close');
								
				click =0;
			}					
		});
		$('.box-refine-xs').click(function(){			
		});
									 
	});
	
</script>
<?php
get_footer();
?>
