<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP Coupon
 */
get_header();

/**
 * Hooks wpcoupon_after_header
 *
 * @see wpcoupon_page_header();
 *
 */
do_action( 'wpcoupon_after_header' );
$layout = wpcoupon_get_site_layout();
?>
    <div id="content-wrap" class="container <?php echo esc_attr( $layout ); ?>">

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div <?php post_class( 'post-entry shadow-box content-box' ); ?>>
                    <?php the_post(); ?>

                    <div class="post-meta">
                        <?php
                        global $authordata;

                        echo '<div class="author-avatar">';
                        echo get_avatar( get_the_author_meta( 'email', $authordata->ID ), 50 );
                        echo '</div>';

                        echo '<div class="post-meta-data">';

                            the_title('<h2 class="post-title">','</h2>');
                            echo '<p class="meta-line-2">';
                            if ( is_object( $authordata ) ) {
                                echo '<span class="author-name">';
                                    printf(
                                        esc_html__( 'Posts by %s' , 'wp-coupon' ),
                                        '<a href="'.esc_url( get_author_posts_url( $authordata->ID, $authordata->user_nicename ) ).'" title="'.esc_attr( sprintf( esc_html__( 'Posts by %s', 'wp-coupon' ), get_the_author() ) ).'" rel="author">'. get_the_author().'</a>'
                                    );
                                echo '</span>';
                            }
                            echo '<span class="comment-number">';
                                comments_number(
                                    esc_html__( '0 Comments', 'wp-coupon' ),
                                    esc_html__( '1 Comment', 'wp-coupon' ),
                                    esc_html__( '% Comments', 'wp-coupon' )
                                );
                            echo '</span>';
                            echo '</p>';


                        echo '</div>';

                        ?>
                    </div>
                    <div class="post-content">
                    <?php the_content(); ?>
                    </div>
                    <?php the_tags( '<div class="entry-tags">', ', ', '</div>' ); ?>
                </div>
                <!--Related Posts-->
                <?php
                     $defaults = array(
                        'numberposts' => 2,
                        'orderby' => 'rand',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                    );
                    $relatedposts= get_posts( $defaults );
                    global $authordata;
                    foreach ( $relatedposts as $post ) :
                        setup_postdata( $post ); 
                        $post_id = get_the_ID();
                        $thumb = get_the_post_thumbnail($post_id, array( 620, 300));
                        $des = get_the_excerpt( $post_id );
                        if(strlen($des) > 150){
                            $desc = substr($des,0,strpos($des, ' ', 150));
                        }else{
                            $desc = $des;
                        }
                        
                        if ( ! is_object( $authordata ) ) {
                            $authordata = get_user_by( 'ID', $post->post_author );
                        }

                ?>
                <div <?php post_class( 'post-entry shadow-box content-box' ); ?>>
                    <?php  if ( $authordata ) { ?>
                    <div class="post-meta">
                        <?php
                        echo '<div class="author-avatar">';
                            echo get_avatar(get_the_author_meta('email', $authordata->ID), 50);
                        echo '</div>';

                        echo '<div class="post-meta-data">';

                            the_title('<h2 class="post-title"><a title="'.esc_attr( get_the_title() ).'" href="'.get_permalink().'"> ', '</a></h2>');
                            echo '<p class="meta-line-2">';
                            if ( is_object( $authordata ) ) {
                                echo '<span class="author-name">';
                                printf(
                                    esc_html__('Posts by %s', 'wp-coupon' ),
                                    sprintf( '<a href="'.esc_url( get_author_posts_url( $authordata->ID, $authordata->user_nicename ) ).'" title="'.esc_attr( sprintf( esc_html__( 'Posts by %s', 'wp-coupon' ), get_the_author() ) ).'" rel="author">'.get_the_author().'</a>' )
                                );
                                echo '</span>';
                            }
                        
                            echo '<span class="comment-number">';
                            comments_number(
                                esc_html__( '0 Comments', 'wp-coupon' ),
                                esc_html__( '1 Comment', 'wp-coupon' ),
                                esc_html__( '% Comments', 'wp-coupon' )
                            );
                            echo '</span>';
                            echo '</p>';

                        echo '</div>'; // .post-meta-data

                        ?>
                    </div>
                    <?php } ?>
                    <?php if ( has_post_thumbnail( ) ) { ?>
                        <div class="shadow-box post-thumbnail">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'wpcoupon_blog_medium' ); ?></a>
                        </div>

                    <?php }  ?>
                <div class="post-content">
                    <a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
                    <!--<a class="read-more tiny ui button btn_primary" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Read More', 'wp-coupon' ) ?></a>-->
                </div>
                <?php  if ( $authordata ) { ?>
                    <div class="post-meta">
                        <?php
                    

                        echo '<div class="post-meta-data">';

                        
                            echo '<p class="meta-line-2 display">';
                            if ( is_object( $authordata ) ) {
                                echo '<span class="author-name">';
                                printf(
                                    esc_html__('By %s', 'wp-coupon' ),
                                    sprintf( '<a href="'.esc_url( get_author_posts_url( $authordata->ID, $authordata->user_nicename ) ).'" title="'.esc_attr( sprintf( esc_html__( 'Posts by %s', 'wp-coupon' ), get_the_author() ) ).'" rel="author">'.get_the_author().'</a>' )
                                );
                                echo '</span>';
                            }
                        
                            echo '<span class="comment-number">';
                            $result = GetTimePost(get_the_date());
                            echo $result;
                            echo '</span>';
                            echo '</p>';

                        echo '</div>'; // .post-meta-data

                        ?>
                    </div>
                    <?php } ?>
                </div>
                <?php endforeach; 
                        wp_reset_postdata();
                    ?>
                <?php

                // wpcoupon_wp_link_pages(  );

                // // If comments are open or we have at least one comment, load up the comment template.
                // if ( comments_open() || get_comments_number() ) :
                //     comments_template();
                // endif;
                ?>


            </main><!-- #main -->
        </div><!-- #primary -->

        <?php

        if ( $layout != 'no-sidebar' ) {
            get_sidebar();
        }

        ?>

    </div> <!-- /#content-wrap -->

<?php get_footer(); ?>
