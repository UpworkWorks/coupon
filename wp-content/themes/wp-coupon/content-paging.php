<?php

$args = array(
    'base'               => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'post_type', get_pagenum_link( 999999999, false ) ) ) ),
    'format'             => '',
    'show_all'           => true,
    'prev_next'          => true,
    'type'               => 'array',
    'add_args'           => false,
    'add_fragment'       => '',
    'before_page_number' => '',
    'after_page_number'  => ''
);
$links = wpcoupon_paginate_links( $args );
$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
$postcount = $wp_query->found_posts;
$current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
if ( $links ) {
    ?>
    
    <script type='text/javascript'>//<![CDATA[
       jQuery(function($){
	         $('#back-to-top').click(function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;
            });
             $('#textbox').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                var str = "";
                if(keycode == '13'){
                    var $number = parseInt($('#textbox').val());
                    var $total = parseInt($('#total').val());
                    if($number < 0 || $number > $total)
                    {
                        $("#error-paging").css('display','block');
                    }
                    else
                    {
                        var $link = document.URL;
                        var arr = $link.split("/");
                        for(i in arr)
                        {
                            if(arr[i] != "page")
                            {
                                if(arr[i]=="")
                                {
                                    str = str;
                                }
                                else {
                                    if(i == 0)
                                    {
                                        str += arr[i] + '//';
                                    }
                                    else {
                                        str += arr[i] + '/';
                                    }
                                    
                                } 
                            }
                            else 
                            {
                                break;
                            }
                            
                        }

                        var linkend = str+'page/'+$number+'/';
                        window.location.href = linkend;
                    }
                    
                                        
                }
                event.stopPropagation();
            });
        });
       
    </script>
    <div class="pagingandtop">
        <div class="ui pagination menu center-div">
            <input type="number" class="number-go-to-page" min="1" max="<?php echo $total ?>" id="textbox" value="<?php echo $current ?>"/>
            
            <span> of   <?php echo " ".$total ?></span>
            <input class="hidden" style="display:none;" value="<?php echo $total ?>" id="total"/>
            <?php foreach ($links as $link) { ?>
                <?php echo wp_kses_post( $link ); ?>
            <?php } ?>
        </div>
        <div class="totalpost">
            (<?php echo $postcount ?> total blog posts)
        </div>
        <div class="gotop" id="back-to-top">
            <a href="#">Back to Top <span></span></a>
        </div>
        <p id="error-paging">
            Page number value must be less than or equal to <?php echo $total ?>.
        </p>
    </div>
    
    <?php
}
