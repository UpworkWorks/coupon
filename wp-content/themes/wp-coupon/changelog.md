## 1.1.0
* Add ref nofollow for coupon code text.
* Improve search.
* Improve coupons search.
* Fix WC out of date template.
* Fix Embed code.
* Add feed for coupons.

## 1.0.9
* Fix favorites store name.
* Fix issue goto store URL in modal.
* Fix coupon readmore details issue.
* Fix issue can not translate "Share" in coupon category.

## 1.0.8
 * Improve coupons listing admin.
 * Improve share email templates.
 * Improve single coupon store.
 * Move coupon submit to plugin.
 * Fix issue count wrong coupon types.
 * Add options to hide coupon thumbnail.
 * Add option to disable feed links.
 * Add an option to hide child stores
 * Add rel="nofollow" for goto store redirect.
 * Add goto store redirect status (default: 301).
 * Update language file.
 * Make search results fit width.


## 1.0.7
* Update Import demo content plugin, now allowed import demo images.
* Fix issue ajax load more coupons.
* Add copy coupon button, support: Chrome 42+, FireFox 41+, Opera 29+, IE9+.
* Setting store,category slug rewrite.
* Fix issue when coupon code too long.
* Update .pot file.
* Update Sematic UI to ver 2.2.1.
* Allow html in store heading.

## 1.0.6
* Add them auto update.
* Fix redirect store url.
* Make store edit input fields wide.
* Update carousel.
* WP Coupon Content Type plugin no longer needed.
* Add Theme styling settings

## 1.0.5
* Fix themeforest review issues.

## 1.0.4
* Fix themeforest review issues.

## 1.0.3
* Fix themeforest review issues.

## 1.0.2
* Fix themeforest review issues.

## 1.0.1
* Fix themeforest review issues.

## 1.0.0
* Initial release.
