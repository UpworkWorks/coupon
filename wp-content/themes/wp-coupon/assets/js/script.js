 jQuery(function($){

		$.post(ajax_object.ajaxurl, {
			action: 'ajax_action'
		}, function(data) {			
			var a = data.slice(0,-1);
            $('.homeblog').html(a);
		});

		
		if($(window).width > 1023)
		{
			var width_content = $('.primary-header .container').width();
			var width_logo = $('.header_right.col-7.fright').width();
			$('.col-5.fleft.logo').width(width_content - width_logo);
		}else if($(window).width <1200){
			var width_content = $('.primary-header .container').width();
			var width_logo = $('.header_right.col-7.fright').width();
			$('.col-5.fleft.logo').width(width_content - width_logo - 30);
		}else if($(window).width <1023){
			var width_content = $('.primary-header .container').width();
			var width_logo = $('.header_right.col-7.fright').width();
			$('.col-5.fleft.logo').width(width_content - width_logo - width_content*0.03);
		}
});