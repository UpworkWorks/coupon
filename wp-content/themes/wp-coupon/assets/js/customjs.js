jQuery(document).ready(function($) {
  $(".filter_coupon").change(function() {
        var values_checked = $("input.filter_coupon:checkbox:checked").map(function(){
            return $(this).val();
        }).toArray();
        if (values_checked.length) {
            $('.items.coupon-item').hide();
            var coupon_ids = [];
            for (var i in values_checked) {
                var list_coupon_id = values_checked[i].split(',');
                coupon_ids = coupon_ids.concat(list_coupon_id.filter(function (item) {
                    return coupon_ids.indexOf(item) < 0;
                }));
            }

            $(".items.coupon-id-"+ coupon_ids.join(',.items.coupon-id-')).show();

        } else {
            $('.items.coupon-item').show();
        }
    });

    $('.category-checklist .seemore').click(function() {
        if ($(this).hasClass('active')) {
            $(this).siblings('.hide-category').hide();
            $(this).removeClass('active').html('+ See More');
        } else {
            $(this).siblings('.hide-category').show();
            $(this).addClass('active').html('- See Less');
        }
    });
})