<?php
$logo_option = wpcoupon_get_option( 'coupon_item_logo' );
if ( $logo_option != 'hide' ) {
    $has_thumb = wpcoupon_coupon()->has_thumb();
    if ( $logo_option !== 'default' ) {
        $has_thumb = has_post_thumbnail( wpcoupon_coupon()->ID );
    } else {
        $has_thumb = true; // just show place holder
    }
} else {
    $has_thumb = false;
}

?>
<div data-id="<?php echo wpcoupon_coupon()->ID; ?>"
     class="coupon-item store-listing-item <?php echo $has_thumb ? 'has-thumb' : 'no-thumb'; ?> c-cat c-type-<?php echo esc_attr( wpcoupon_coupon()->get_type() ); ?>">
    <div class="pleft-15 shadow-box"  >        
        <?php if ( $has_thumb ) { 
            $post_meta = get_post_meta(wpcoupon_coupon()->ID, '_wpc_coupon_image');
            $img = "";
            if(!empty($post_meta)) {
                $img= $post_meta[0];
            } else {
                $img = "http://www.offer.me/wp-content/themes/wp-coupon/assets/images/deal_img_default.jpg";
            }
            ?>
        <div class="store-thumb-link">
            <div class="store-thumb center-div">
                <a href="<?php echo esc_attr( wpcoupon_coupon()->get_store_url() ); ?>" style="background-image:url(<?php echo($img);?>); 
                                                                                                background-position: center;
                                                                                                background-size: cover;">
                </a>
            </div>
            <div class="store-icon">
                <a class="center-div" href="<?php echo esc_attr( wpcoupon_coupon()->get_store_url() ); ?>">
                    <?php
                        echo wpcoupon_coupon()->get_store_thumb( 'wpcoupon_small_thumb' );
                    ?>
                </a>
            </div>
            <div class="store-name"><a href="<?php echo wpcoupon_coupon()->get_store_url(); ?>"><?php printf( esc_html__( '%s Coupons', 'wp-coupon' ), wpcoupon_coupon()->store->name ); ?><i class="angle right icon"></i></a></div>
        </div>
        <?php } ?>

        <div class="latest-coupon">
            <div class="store-icon">
                    <a class="center-div" href="<?php echo esc_attr( wpcoupon_coupon()->get_store_url() ); ?>">
                        <?php
                            echo wpcoupon_coupon()->get_store_thumb( 'wpcoupon_small_thumb' );
                        ?>
                    </a>
            </div>
            <div class="store-info">
                <h3 class="coupon-title" >
                    <?php
                    edit_post_link('<i class="edit icon"></i>', '', '', wpcoupon_coupon()->ID );
                    ?>
                    <a class="coupon-link" rel="nofollow"
                    title="<?php echo esc_attr( get_the_title( wpcoupon_coupon()->ID ) ) ?>"
                    data-type="<?php echo wpcoupon_coupon()->get_type(); ?>"
                    data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>"
                    data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>"
                    data-code="<?php echo esc_attr( wpcoupon_coupon()->get_code() ); ?>"
                    href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>">
                        <?php 
                        
                            if(strlen(get_the_title( wpcoupon_coupon()->ID )) < 30)
                            {
                                echo substr(get_the_title( wpcoupon_coupon()->ID),0,30) ;
                            }
                            else{
                                echo substr(get_the_title( wpcoupon_coupon()->ID ),0,30).'...' ;
                            }
                        ?>
                    </a>
                </h3>
                <div class="coupon-des">
                    <div class="coupon-des-ellip"><?php
                        echo  wpcoupon_coupon()->get_excerpt(
                                false,
                                '<span class="c-actions-span">...<a class="more" href="#">'.esc_html__( 'More', 'wp-coupon' ).'</a></span>',
                                $has_thumb
                            );
                        ?></div>
                    <?php
                    if ( wpcoupon_coupon()->has_more_content ) { ?>
                    <div class="coupon-des-full"><?php
                        echo str_replace(']]>', ']]&gt;', apply_filters('the_content', wpcoupon_coupon()->post_content . ' <a class="more less" href="#">' . esc_html__('Less', 'wp-coupon') . '</a>'));
                        ?></div>
                    <?php } ?>
                </div>
                <?php 
                    if ( ! wpcoupon_coupon()->has_expired() ) { 
                        $date_coupon =  GetTimePost(wpcoupon_coupon()->get_expires()); 
                    } else { 
                        $date_coupon =   GetTimePost(wpcoupon_coupon()->get_expires()) ; 
                    } 
                    if (strpos($date_coupon,'ago') !== false) {?>
                         <div class="exp-text">
                            Expires: <span class="outdate"><?php echo($date_coupon); ?></span>
                            <a data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( "Save this coupon", 'wp-coupon' ); ?>" href="#" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" class="add-coupon-favorite coupon-save icon-popup"><i class="empty star icon"></i>Save</a>
                        </div>
                    <?php
                    }else{?>
                         <div class="exp-text">
                            Expires: <?php echo($date_coupon); ?>
                            <a data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( "Save this coupon", 'wp-coupon' ); ?>" href="#" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" class="add-coupon-favorite coupon-save icon-popup"><i class="empty star icon"></i>Save</a>
                        </div>
                    <?php
                    }
                ?>

               
            </div>
        </div>

        <div class="coupon-detail coupon-button-type">
            <?php
            switch ( wpcoupon_coupon()->get_type() ) {

                case 'sale':
                    ?>
                    <a rel="nofollow" data-type="<?php echo wpcoupon_coupon()->get_type(); ?>" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>" class="coupon-deal coupon-button" href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>"><?php esc_html_e( 'Get Deal', 'wp-coupon' ); ?> <i class="shop icon"></i></a>
                    <?php
                    break;
                case 'print':
                    ?>
                    <a rel="nofollow" data-type="<?php echo wpcoupon_coupon()->get_type(); ?>" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>" class="coupon-print coupon-button" href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>"><?php esc_html_e( 'Print Coupon', 'wp-coupon' ); ?> <i class="print icon"></i></a>
                    <?php
                    break;
                default:
                    ?>
                    <a rel="nofollow" data-type="<?php echo wpcoupon_coupon()->get_type(); ?>"
                    data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>"
                    href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>"
                    class="coupon-button coupon-code"
                    data-tooltip="<?php echo esc_attr_e( 'Click to copy & open site', 'wp-coupon' ); ?>"
                    data-position="top center"
                    data-inverted=""
                    data-code="<?php echo esc_attr( wpcoupon_coupon()->get_code() ); ?>"
                    data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>">
                        <span class="code-text" rel="nofollow"><?php echo esc_html( wpcoupon_coupon()->get_code( 8 ) ); ?></span>
                        <div class="getcode-hover">
                            <span class="get-code"><?php  esc_html_e( 'Get Code', 'wp-coupon' ); ?></span>
                            <span class="arrow-code"></span>
                        </div>
                    </a>
                    <?php
            }
            ?>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <?php
        //get_template_part('loop/coupon-modal');
        ?>
    </div>
</div>
