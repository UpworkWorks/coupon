<!-- Coupon Modal -->
<?php
    wpcoupon_setup_store();
    $current_link =  get_permalink();
    $store_id = WPCoupon_Coupon()->get_store_id();
    if ( !empty( $store_id ) ) {
        $stores = wpcoupon_get_stores( array( 'include'=> $store_id ) );
        if ( $stores ) {
            foreach ( $stores as $store ){
                $store_name = esc_html( $store->name );
            }
        }
    }
?>
<div data-modal-id="<?php echo wpcoupon_coupon()->ID; ?>" class="ui modal coupon-modal coupon-code-modal">
    <div class="coupon-header clearfix">
        <div class="coupon-content">
            <p class="coupon-type-text">
                <?php
                switch ( wpcoupon_coupon()->get_type() ) {
                    case 'sale':
                        esc_html_e( 'no coupon code required', 'wp-coupon' );
                        break;
                    case 'print':
                        esc_html_e( 'Print this coupon and redeem it in-store', 'wp-coupon' );
                        break;
                    default:
                        esc_html_e( 'Copy this code', 'wp-coupon' );
                }
                ?>
            </p>
            <span>
                 <?php
                switch ( wpcoupon_coupon()->get_type() ) {
                    case 'sale':
                        
                        break;
                    case 'print':
                        
                        break;
                    default:
                        echo preg_replace('/&apos;|&amp;apos;/', "'",  esc_attr( 'Paste this code at checkout to get an extra '.get_the_title( wpcoupon_coupon()->ID ), 'wp-coupon' ) );
                }
                ?>
            </span>
            
                <?php
                switch ( wpcoupon_coupon()->get_type() ) {

                    case 'sale':
                        ?>
                        <div class="modal-code">
                            <a class="ui button btn btn_secondary go-store" target="_blank" rel="nofollow" href="<?php echo esc_attr( wpcoupon_coupon()->get_go_store_url() ); ?>">Go to <?php echo  $store_name; ?> and Save Now!<i class="angle right icon"></i></a>
                        </div>
                        <?php
                        break;
                    case 'print':
                        $image_url = esc_url( wpcoupon_coupon()->get_print_image() );
                        ?>
                        <div class="modal-code">
                            <a class="btn-print-coupon" target="_blank" href="<?php echo esc_attr( $image_url ); ?>"><img alt="" src="<?php echo esc_attr( $image_url ); ?>"/></a>
                        </div>
                        <?php
                        break;
                    default:
                        ?>
                        <div class="modal-code">
                            <div class="coupon-code">
                                <div class="ui fluid action input massive">
                                    <input  type="text" class="code-text" autocomplete="off" readonly value="<?php echo esc_attr( wpcoupon_coupon()->get_code() ); ?>">
                                    <button class="ui right labeled icon button btn btn_secondary">
                                        <!--<i class="copy icon"></i>-->
                                        <span><?php esc_html_e( 'Copy', 'wp-coupon' ); ?></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <span class="user-ratting-text"><?php esc_html_e( 'Did this coupon work for you?  ', 'wp-coupon' ); ?></span>
                            <div class="user-ratting ui icon basic buttons">
                                <div class="ui button icon-popup coupon-vote" data-vote-type="up" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( 'This worked', 'wp-coupon' ); ?>"><i class="smile icon"></i></div>
                                <div class="ui button icon-popup coupon-vote" data-vote-type="down" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( "It didn't work", 'wp-coupon' ); ?>"><i class="frown icon"></i></div>
                                <div class="ui button icon-popup coupon-save" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( "Save this coupon", 'wp-coupon' ); ?>"><i class="empty star icon"></i>Save</div>
                                <div class="ui button icon-popup coupon-save" data-position="top center" >
                                <a class="modal-share" href="#"><i class="share alternate icon"></i> <?php esc_html_e( 'Share', 'wp-coupon' ); ?></a>
                                </div>
                            </div>
                        </div>
                    <?php
                }
                ?>
            
            
            <div class="clearfix" style="border-bottom: 1px solid rgba(102,102,102,0.2);">
                <?php if ( wpcoupon_coupon()->get_type() !== 'sale' ) { ?>
                    <?php if ( wpcoupon_coupon()->get_type() == 'print' ) { ?>
                        <a class="ui button btn btn_secondary go-store btn-print-coupon"  href="<?php echo esc_attr( $image_url ); ?>"><?php esc_html_e( 'Print Now', 'wp-coupon' ); ?> <i class="print icon"></i></a>
                    <?php } else { ?>
                        <a href="<?php echo esc_attr( wpcoupon_coupon()->get_go_store_url() ); ?>" rel="nofollow" target="_blank" class="ui button btn btn_secondary go-store">
                            Go to <?php echo( $store_name); ?><i class="angle right icon"></i>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="clearfix" style="padding: 35px 0 13px 0;">
                <div class="coupon-store-thumb">
                    <a href="<?php echo esc_attr( wpcoupon_coupon()->get_go_store_url() ); ?>" rel="nofollow" target="_blank" class="ui button btn btn_secondary go-store">
                        <?php echo wpcoupon_coupon()->get_thumb( ); ?>
                    </a>
                    
                </div> 
                <div class="content-modal">
                    <div class="coupon-title" title="<?php echo esc_attr( get_the_title( wpcoupon_coupon()->ID ) ) ?>">Save up to <?php echo get_the_title( wpcoupon_coupon()->ID ); ?></div>
                    <span class="show-detail"><a href="#"><i class="angle down icon"></i><?php esc_html_e( 'See Details', 'wp-coupon' ) ?></a></span>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="coupon-popup-detail">
                <div class="coupon-detail-content"><?php
                    echo str_replace( ']]>', ']]&gt;', apply_filters( 'the_content', wpcoupon_coupon()->post_content ) );  ;
                    ?></div>
                <p><strong><?php esc_html_e( 'Expires', 'wp-coupon' ); ?></strong>: <?php echo wpcoupon_coupon()->get_expires( null, true ); ?></p>
                <p><strong><?php esc_html_e( 'Submitted', 'wp-coupon' ); ?></strong>:
                    <?php printf( esc_html__( '%s ago', 'wp-coupon' ), human_time_diff( get_the_time('U',wpcoupon_coupon()), current_time('timestamp') ) ); ?>
                </p>
            </div>
        </div>
        <!--<div class="coupon-store-thumb">
            <?php
            echo wpcoupon_coupon()->get_thumb( );
            ?>
        </div> 
        <div class="coupon-title" title="<?php echo esc_attr( get_the_title( wpcoupon_coupon()->ID ) ) ?>"><?php echo get_the_title( wpcoupon_coupon()->ID ); ?></div>-->
        <span class="cancel close"></span>
    </div>
    
    <div class="coupon-footer">
        <!--<ul class="clearfix">
            <li><span><i class="wifi icon"></i> <?php printf( esc_html__( '%1$s Used - %2$s Today', 'wp-coupon' ), wpcoupon_coupon()->get_total_used(), wpcoupon_coupon()->get_used_today() ); ?></span></li>
            <li></li>
        </ul>-->
        <div class="clearfix">
            <?php
                if ( ! is_active_sidebar( 'newsletter-modal' ))
                    return;
            ?>
            <?php
                    $postfooter_sidebars = array( 'newsletter-modal' );
                    foreach ( $postfooter_sidebars as $key => $postfooter_sidebar ){
                        if ( is_active_sidebar( $postfooter_sidebar ) ) {
                            echo '<div class="postfooter-widget' . (  2 == $key ? ' last' : '' ) . '">';
                            echo '<div class="newsletter-text before-form">Get the best coupons for '.$store_name.' and other top stores.</div>';
                            dynamic_sidebar( $postfooter_sidebar );
                            echo '</div> <!-- end .postfooter-widget -->';
                        }
                    }
                ?>

        </div>
        <div class="share-modal-popup ui popup">
            
            <?php
            
            $args =  array(
                'title'     => get_the_title( wpcoupon_coupon()->ID ),
                'summary'   => wpcoupon_coupon()->get_excerpt(140),
                'url'       => wpcoupon_coupon()->get_share_url().'#c-'.wpcoupon_coupon()->ID
            );
            echo WPCoupon_Socials::facebook_share( $args );
            echo WPCoupon_Socials::twitter_share( $args );

            do_action('loop_coupon_more_share_buttons');

            ?>
        </div>
    </div>
</div>