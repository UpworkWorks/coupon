<div class="items coupon-listing-item store-listing-item coupon-item shadow-box sixteen coupon-id-<?php echo wpcoupon_coupon()->ID; ?>">		     				
     <div class="ui grid">
     	<div class="three wide column">
		 	<div class="content-deal-tyle">
			 	<a class="ui button btn btn_secondary go-store" target="_blank" rel="nofollow" href="<?php echo esc_attr( wpcoupon_coupon()->get_store_url()  ); ?>">
					<div class="deal-type">
						<?php 
							$store_id = WPCoupon_Coupon()->get_store_id();
							$stores = wpcoupon_get_stores( array(  'include'=> $store_id ) );
							echo wpcoupon_coupon()->get_thumb( );
						?>
					</div>
				</a>
			</div>
     	</div>
     	<div class="thirteen wide column">
     		<div class="ui grid">		
			 	<div class="sixteen wide column pdbt0 deal-type"><a href="#" class="tag-code">PROMO CODE</a> <div class="store-name">
				 	<?php 
						echo wpcoupon_coupon()->store->get_display_name();
					?>
				 </div> </div>     									     									     								
     			<div class="eleven wide column">
     				<h2>
     					<?php echo preg_replace('/&apos;|&amp;apos;/', "'", esc_attr( get_the_title( wpcoupon_coupon()->ID ) ) ) ?>
     				</h2>
     				<span class="author"> Submitted by: 
					 	<span class="underline">
						 <?php $author_id = get_post( wpcoupon_coupon()->ID )->post_author; 
     							$authord = get_the_author_meta( 'nicename', $author_id); 
     							echo $authord ;
							?>
						</span>
					</span>
					 <?php
					 	if(str_word_count(get_the_content()) > 15){
						 	$subdecs= wp_trim_words( get_the_content(), 15 );
					 ?>
					 		<div class="coupon-des">
								<div class="coupon-des-ellip"><?php echo($subdecs) ?>
									<span class="c-actions-span"><a class="more" href="#">+ See More</a></span>
								</div>
								<div class="coupon-des-full"><p><?php echo(get_the_content()) ?> <span class="c-actions-span"> <a class="more less" href="#">Less</a></span></p>
								</div>
							</div>
					 <?php
						} else{ 
							$subdecs = get_the_content();
					 ?>
					 	   <div class="coupon-des">
								<div class="coupon-des-ellip">
									<p><?php echo(get_the_content()) ?> </p>
								</div>
							</div>
					 <?php
						} 
					 ?>
     			</div>
     			<div class="five wide column">
     				<div class="coupon-detail coupon-button-type">
     					<?php
					        switch ( wpcoupon_coupon()->get_type() ) {
					
					            case 'sale':
					                ?>
					                <a rel="nofollow" data-type="<?php echo wpcoupon_coupon()->get_type(); ?>" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>" class="coupon-deal coupon-button" href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>"><?php esc_html_e( 'Get Deal', 'wp-coupon' ); ?> <i class="shop icon"></i></a>
					                <?php
					                break;
					            case 'print':
					                ?>
					                <a rel="nofollow" data-type="<?php echo wpcoupon_coupon()->get_type(); ?>" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>" class="coupon-print coupon-button" href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>"><?php esc_html_e( 'Print Coupon', 'wp-coupon' ); ?> <i class="print icon"></i></a>
					                <?php
					                break;
					            default:
					                ?>
					                <a rel="nofollow" data-type="code" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" href="<?php echo esc_attr( wpcoupon_coupon()->get_href() ); ?>" class="coupon-button coupon-code" data-tooltip="Click to copy &amp; open site" data-position="top center" data-inverted="" data-code="<?php echo esc_attr( wpcoupon_coupon()->get_code() ); ?>" data-aff-url="<?php echo esc_attr( wpcoupon_coupon()->get_go_out_url() ); ?>">
					                    <span class="code-text" rel="nofollow"><?php echo esc_html( wpcoupon_coupon()->get_code( 8 ) ); ?></span>
										<div class="getcode-hover">
					                    	<span class="get-code"><?php  esc_html_e( 'Get Code', 'wp-coupon' ); ?></span>
											<span class="arrow-code"></span>
										</div>
					                </a>
					                <?php
					        }
					    ?>
                        										                        										    
				    </div>
			    </div>
			    <div class="sixteen wide column pdbt0">
					<div class="user-ratting ui icon basic buttons coupon-footer coupon-listing-footer">
						<?php 
							$percent_success_value = wpcoupon_coupon()->percent_success();
							if ($percent_success_value !== null) {
								echo '<span class="voted-value">';
								printf( esc_html__( '%s%% Success', 'wp-coupon' ), round( wpcoupon_coupon()->percent_success() ) );
								echo '</span>';
							}
						?>
						Did this coupon work for you?
						<div class="ui button icon-popup coupon-vote thum-up" data-vote-type="up" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>"  data-position="top center" data-inverted="" data-tooltip="<?php esc_attr_e( 'This worked', 'wp-coupon' ); ?>"><i class="smile icon"></i></div>
						<div class="ui button icon-popup coupon-vote thum-down border-right" data-vote-type="down" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>"  data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( "It didn't work", 'wp-coupon' ); ?>"><i class="frown icon"></i></div>
						<div class="share-email">
							<div class="ui button icon-popup coupon-vote border-right" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>"><a title="<?php esc_attr_e( 'Share it with your friend', 'wp-coupon' ); ?>" data-reveal="reveal-share" href="#"><i class="share alternate icon"></i> <?php esc_html_e( 'Share', 'wp-coupon' ); ?></a></div>
							<div class="ui button icon-popup coupon-vote border-right" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>"><a title="<?php esc_attr_e( 'Send this coupon to an email', 'wp-coupon' ); ?>" data-reveal="reveal-email" href="#"><i class="mail outline icon"></i> <?php esc_html_e( 'Email', 'wp-coupon' ); ?></a></div>
							<div class="ui button icon-popup coupon-save" data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" data-position="top center" data-inverted=""  data-tooltip="<?php esc_attr_e( "Save this coupon", 'wp-coupon' ); ?>" ><i class="empty star icon"></i> Save</div>
							<div data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" class="reveal-content reveal-share">
								<span class="close"></span>
								<h4><?php esc_html_e( 'Share it with your friends', 'wp-coupon' ); ?></h4>
								<div class="ui fluid left icon input">
									<input value="<?php echo wpcoupon_coupon()->get_share_url() ?>#c-<?php echo wpcoupon_coupon()->ID; ?>" type="text">
									<i class="linkify icon"></i>
								</div>
								<br>
								<div class="coupon-share">
									<?php
									$args =  array(
										'title'     => get_the_title( wpcoupon_coupon()->ID ),
										'summary'   => wpcoupon_coupon()->get_excerpt(140),
										'url'       => wpcoupon_coupon()->get_share_url().'#c-'.wpcoupon_coupon()->ID
									);
									echo WPCoupon_Socials::facebook_share( $args );
									echo WPCoupon_Socials::twitter_share( $args );

									do_action('loop_coupon_more_share_buttons');

									?>
								</div>
							</div>
							<div data-coupon-id="<?php echo wpcoupon_coupon()->ID; ?>" class="reveal-content reveal-email">
								<span class="close"></span>
								<h4 class="send-mail-heading"><?php esc_html_e( 'Send this coupon to an email', 'wp-coupon' ); ?></h4>
								<div class="ui fluid action left icon input">
									<input class="email_send_to" placeholder="<?php esc_attr_e( 'Email address ...', 'wp-coupon' ); ?>" type="text">
									<i class="mail outline icon"></i>
									<div class="email_send_btn ui button btn btn_primary"><?php esc_html_e( 'Send', 'wp-coupon' ); ?></div>
								</div><br>
								<p><?php esc_html_e( 'This is not a email subscription service. Your email (or your friend\'s email) will only be used to send this coupon.', 'wp-coupon' ); ?></p>
							</div> 
						</div>
					</div>
			    </div>
			    
			    <div class="sixteen2-xs wide column pdbt0">
			    	<ul class="line-bottom2">									    		
			    		<li><a href="#"><i class="share alternate icon"></i> Share</a></li>
			    		<li><a href="#"><i class="mail outline icon"></i> Email</a></li>
			    		<li><a href="#"><i class="empty star icon"></i> Save</a></li>
			    	</ul>
			    </div>
			    
     		</div>		     						
     	</div>
     	 
     </div>
	     					
     </div>
