<?php
/**
* Add Popular Store widget.
*/
class WPCoupon_Popular_Store extends WP_Widget {
    /**
    * Register widget with WordPress.
    */
    function __construct() {
        parent::__construct(
            'popular_stores', // Base ID
            esc_html__( 'WPCoupon Popular Stores', 'wp-coupon' ), // Name
            array( 'description' => esc_html__( 'Display Popular Stores', 'wp-coupon' ), ) // Args
        );
    }

    /**
    * Front-end display of widget.
    *
    * @see WP_Widget::widget()
    *
    * @param array $args     Widget arguments.
    * @param array $instance Saved values from database.
    */
    public function widget( $args, $instance ) {

        $args = wp_parse_args( $args, array(
            'before_widget' => '',
            'after_widget' => '',
            'after_title' => '',
        ) );

        echo $args['before_widget'];

        $instance =  wp_parse_args( $instance, array(
            'title'         => '',
            'number'        => 6,
            'item_per_row'  => 2,
        ) );

        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        $tax_args = array(
            'orderby'                => 'count',
            'order'                  => 'DESC',
            'hide_empty'             => false,
            'include'                => array(),
            'exclude'                => array(),
            'exclude_tree'           => array(),
            'number'                 => $instance['number'],
            'hierarchical'           => false,
            'pad_counts'             => false,
            'child_of'               => 0,
            'childless'              => false,
            'cache_domain'           => 'core',
            'update_term_meta_cache' => true,
        );

        $stores = get_terms( 'coupon_store', $tax_args );

        global $post;
        ?>
        <div class="widget-content shadow-box">
            <div class="ui <?php echo wpcoupon_number_to_html_class( $instance['item_per_row'] ); ?> column grid">
                <?php
                foreach ( $stores as $store ) {
                    wpcoupon_setup_store( $store );
                    ?>
                <div class="column">
                    <div class="store-thumb">
                        <a class="ui image middle aligned" href="<?php echo wpcoupon_store()->get_url(); ?>">
                            <div class="popular-title"><?php echo wpcoupon_store()->get_display_name() ?></div>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php
       // wp_reset_postdata();
        echo $args['after_widget'];
    }

    /**
    * Back-end widget form.
    *
    * @see WP_Widget::form()
    *
    * @param array $instance Previously saved values from database.
    */
    public function form( $instance ) {

        $instance =  wp_parse_args( $instance, array(
            'title'         => '',
            'number'        => 6,
            'item_per_row' => 2,
        ) );
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Popular Stores', 'wp-coupon' );

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' ,'wp-coupon' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e( 'Number store to show:', 'wp-coupon' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr( $instance['number'] ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'item_per_row' ); ?>"><?php esc_html_e( 'Number item per row:', 'wp-coupon' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'item_per_row' ); ?>">
            <?php for (  $i = 1; $i <=16 ; $i++ ) {
                echo '<option value="'.$i.'" '.selected( $instance['item_per_row'], $i, false ).' >'.$i.'</option>';
            } ?>
            </select>

        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        //$instance = array();
        $new_instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $new_instance;
       // return $instance;
    }

} // class Popular_Store

// register Foo_Widget widget
function wpcoupon_register_popular_store_widget() {
    register_widget( 'WPCoupon_Popular_Store' );
}
add_action( 'widgets_init', 'wpcoupon_register_popular_store_widget' );