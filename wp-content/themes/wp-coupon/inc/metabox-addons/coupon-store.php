<?php
/*
* Custom field for CMB2 which adds a post-search dialog for searching/attaching other post IDs
*/

function cmb2_coupon_store_render_field( $field, $escaped_value, $object_id, $object_type, $field_type ) {

    // get the post type that user selected
    if ( $escaped_value == '' || $escaped_value <= 0 ) {
        $title = '';
    } else {
        $title =  get_the_title( $escaped_value );
    }

    if ( ! $title ){
        $title = esc_html__('[No store selected]', 'wp-coupon' );
    }

    echo '<span class="cs-post-name">'.$title.'</span>';
    echo $field_type->input( array(
        'type'            => 'hidden',
        'class'           => 'coupon_store_input',
        'data-posttype'   => 'store',
        'data-selecttype' => 'radio',
    ) );
}
add_action( 'cmb2_render_coupon_store', 'cmb2_coupon_store_render_field', 15, 5 );

function cmb2_coupon_store_render_js(  $cmb_id, $object_id, $object_type, $cmb ) {
    static $cs_rendered;

    if ( $cs_rendered ) {
        return;
    }

    $fields = $cmb->prop( 'fields' );

    if ( ! is_array( $fields ) ) {
        return;
    }

    $has_post_search_field = false;
    foreach ( $fields as $field ) {
        if ( 'coupon_store' == $field['type'] ) {
            $has_post_search_field = true;
            break;
        }
    }

    if ( ! $has_post_search_field ) {
        return;
    }

    // JS needed for modal
    // wp_enqueue_media();
    wp_enqueue_script( 'wp-backbone' );

    if ( ! is_admin() ) {
        // Will need custom styling!
        // @todo add styles for front-end
        require_once( ABSPATH . 'wp-admin/includes/template.php' );
        do_action( 'cmb2_coupon_store_field_add_find_posts_div' );
    }

    // markup needed for modal
    add_action( 'admin_footer', 'cmb2_wpc_find_store_div' );


    $error = esc_html__( 'An error has occurred. Please reload the page and try again.', 'wp-coupon' );
    $find  = esc_html__( 'Change store', 'wp-coupon' );

    // @TODO this should really be in its own JS file.
    ?>
    <script type="text/javascript">

        jQuery(document).ready(function($){
            'use strict';

            var l10n = {
                'error' : '<?php echo esc_js( $error ); ?>',
                'find' : '<?php echo esc_js( $find ) ?>'
            };


            var SearchView = window.Backbone.View.extend({
                el         : '#find-posts',
                overlaySet : false,
                $overlay   : false,
                $idInput   : false,
                $label     : false,
                $checked   : false,

                events : {
                    'keypress .find-box-search :input' : 'maybeStartSearch',
                    'keyup #find-posts-input'  : 'escClose',
                    'click #find-posts-search' : 'send',
                    'click #find-posts-close'  : 'close',
                    'click tr.found-posts'     : 'selectedPost',
                },

                initialize: function() {
                    this.$spinner  = this.$el.find( '.find-box-search .spinner' );
                    this.$input    = this.$el.find( '#find-posts-input' );
                    this.$response = this.$el.find( '#find-posts-response' );
                    this.$overlay  = $( '.ui-find-overlay' );

                    this.listenTo( this, 'open', this.open );
                    this.listenTo( this, 'close', this.close );
                },

                escClose: function( evt ) {
                    if ( evt.which && 27 === evt.which ) {
                        this.close();
                    }
                },

                close: function() {
                    this.$overlay.hide();
                    this.$el.hide();
                },

                open: function() {
                    this.$response.html('');

                    this.$el.show();

                    this.$input.focus();

                    if ( ! this.$overlay.length ) {
                        $( 'body' ).append( '<div class="ui-find-overlay"></div>' );
                        this.$overlay  = $( '.ui-find-overlay' );
                    }

                    this.$overlay.show();

                    // Pull some results up by default
                    this.send();

                    return false;
                },

                maybeStartSearch: function( evt ) {
                    if ( 13 == evt.which ) {
                        this.send();
                        return false;
                    }
                },

                send: function() {

                    var search = this;
                    search.$spinner.show();

                    $.ajax( ajaxurl, {
                        type     : 'POST',
                        dataType : 'json',
                        data     : {
                            ps               : search.$input.val(),
                            action           : 'wpcoupon_find_posts',
                            cmb2_post_search : true,
                            post_search_cpt  : search.postType,
                            _ajax_nonce      : $('#_ajax_nonce').val()
                        }
                    }).always( function() {
                        search.$el.find( '.find-box-buttons').remove();
                        search.$el.find( '.find-box-inside').css('bottom', '15px' );
                        search.$spinner.hide();

                    }).done( function( response ) {

                        if ( ! response.success ) {
                            search.$response.text( l10n.error );
                        }

                        var data = response.data;

                        if ( 'checkbox' === search.selectType ) {
                            data = data.replace( /type="radio"/gi, 'type="checkbox"' );
                        }

                        search.$response.html( data );
                        search.$el.find( '.found-radio').hide();

                    }).fail( function() {
                        search.$response.text( l10n.error );
                    });
                },

                selectedPost: function( evt, a, b, c ){
                    evt.preventDefault();
                    var tg      = $( evt.currentTarget );
                    var id      = $('.found-radio input', tg ).val();
                    var label   = $( 'label[for="found-'+id+'"]').text();
                    this.$idInput.val( id );
                    this.$label.html( label );
                    this.close();
                }

            });

            window.cmb2_post_search = new SearchView();

            $( '.cmb-type-coupon-store .cmb-td .cs-post-name' ).after( '<span title="'+ l10n.find +'" style="color: #999; margin-left: 5px; cursor: pointer;" class="stcs-open-search dashicons dashicons-edit"></span>');

            $( '.cmb-type-coupon-store .cmb-td .stcs-open-search' ).on( 'click', openSearch );

            function openSearch( evt ) {
                var search = window.cmb2_post_search;
                search.$idInput   = $( evt.currentTarget ).parents( '.cmb-type-coupon-store' ).find( '.cmb-td input.coupon_store_input' );
                search.postType   = search.$idInput.data( 'posttype' );
                search.selectType = 'radio' == search.$idInput.data( 'selecttype' ) ? 'radio' : 'checkbox';
                search.$label     =  $( evt.currentTarget ).parents( '.cmb-type-coupon-store' ).find( '.cmb-td .cs-post-name' );
                search.trigger( 'open' );
            }
            
        });
    </script>
    <?php

    $cs_rendered = true;
}
add_action( 'cmb2_after_form', 'cmb2_coupon_store_render_js', 20, 4 );

/**
 * Add the find posts div via a hook so we can relocate it manually
 */
function cmb2_coupon_store_field_add_find_posts_div() {
    add_action( 'wp_footer', 'find_posts_div' );
}
add_action( 'cmb2_coupon_store_field_add_find_posts_div', 'cmb2_coupon_store_field_add_find_posts_div' );



/**
 * {@internal Missing Short Description}}
 *
 * @since 2.7.0
 *
 * @param string $found_action
 */
function cmb2_wpc_find_store_div($found_action = '') {
    ?>
    <div id="find-posts" class="find-box" style="display: none;">
        <div id="find-posts-head" class="find-box-head">
            <?php esc_html_e( 'Find Stores', 'wp-coupon' ); ?>
            <div id="find-posts-close"></div>
        </div>
        <div class="find-box-inside">
            <div class="find-box-search">
                <?php if ( $found_action ) { ?>
                    <input type="hidden" name="found_action" value="<?php echo esc_attr($found_action); ?>" />
                <?php } ?>
                <input type="hidden" name="affected" id="affected" value="" />
                <?php wp_nonce_field( 'find-posts', '_ajax_nonce', false ); ?>
                <label class="screen-reader-text" for="find-posts-input"><?php esc_html_e( 'Search', 'wp-coupon' ); ?></label>
                <input type="text" id="find-posts-input" name="ps" value="" />
                <span class="spinner"></span>
                <input type="button" id="find-posts-search" value="<?php esc_attr_e( 'Search', 'wp-coupon' ); ?>" class="button" />
                <div class="clear"></div>
            </div>
            <div id="find-posts-response"></div>
        </div>
    </div>
<?php
}




/**
 * Ajax handler for querying posts for the Find Posts modal.
 *
 * @see window.findPosts
 *
 * @since 3.1.0
 */
function cmb2_store_find_posts() {
    check_ajax_referer( 'find-posts' );

    //$post_types = get_post_types( array( 'public' => true ), 'objects' );
    //unset( $post_types['attachment'] );

    $s = wp_unslash( $_POST['ps'] );
    $args = array(
        'post_type'         => 'store',
        'post_status'       => 'publish',
        'posts_per_page'    => 50,
        'orderby'           => 'title',
        'order'             => 'asc',
    );
    if ( '' !== $s )
        $args['s'] = $s;

    $posts = get_posts( $args );

    if ( ! $posts ) {
        wp_send_json_error( esc_html__( 'No items found.' , 'wp-coupon') );
    }

    $html = '<table class="widefat"><thead><tr><th class="found-radio"><br /></th><th>'.esc_html__( 'Store', 'wp-coupon').'</th><th class="no-break">'.esc_html__( 'Date', 'wp-coupon' ).'</th></tr></thead><tbody>';
    $alt = '';
    foreach ( $posts as $post ) {
        $title = trim( $post->post_title ) ? $post->post_title : esc_html__( '(no title)', 'wp-coupon' );
        $alt = ( 'alternate' == $alt ) ? '' : 'alternate';

        if ( '0000-00-00 00:00:00' == $post->post_date ) {
            $time = '';
        } else {
            /* translators: date format in table columns, see http://php.net/date */
            $time = mysql2date(esc_html__('Y/m/d', 'wp-coupon'), $post->post_date);
        }

        $html .= '<tr class="' . trim( 'found-posts ' . $alt ) . '"><td class="found-radio"><input type="radio" id="found-'.$post->ID.'" name="found_post_id" value="' . esc_attr($post->ID) . '"></td>';
        $html .= '<td><label for="found-'.$post->ID.'">' . esc_html( $title ) . '</label></td>
        <td class="no-break">'.esc_html( $time ) . '</td></tr>' . "\n\n";
    }

    $html .= '</tbody></table>';

    wp_send_json_success( $html );
}

add_action( 'wp_ajax_wpcoupon_find_posts', 'cmb2_store_find_posts' );
add_action( 'wp_ajax_nopriv_wpcoupon_find_posts', 'cmb2_store_find_posts' );