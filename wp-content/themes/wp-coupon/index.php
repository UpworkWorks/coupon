<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP Coupon
 */
get_header();

/**
 * Hooks wpcoupon_after_header
 *
 * @see wpcoupon_page_header();
 *
 */
do_action( 'wpcoupon_after_header' );
$layout = wpcoupon_get_site_layout();

?>
    <div id="content-wrap" class="container-index <?php echo esc_attr( $layout ); ?>">
       <!--Add featured post-->
        <div class="featured-post margin-8 white-back">
            <div class="container">
                <div class="inner">
                    <div class="inner-content clearfix">
                        <div class="header-content">
                            <h1>Seasonal Halloween Blog Title</h1>                           
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php 
                        $count=0;
                        $featured = featuredPost(6,'featured');
                
                        foreach ( $featured as $post ) :
                        setup_postdata( $post ); 
                        $count = $count + 1 ;
                        $post_id = get_the_ID();
                        $thumb = get_the_post_thumbnail($post_id, array( 620, 300));
                        $des =$post->post_excerpt;
                        $desc = "";
                        $desc_lenght = strlen($des);
                        if($desc_lenght==0){
                            $desc="";
                        }else{
                            if($desc_lenght < 100)
                                {
                                    $desc = $des.'<i class="angle right icon"></i>' ;
                                }else{
                                $x = 100;
                                   $lines = explode("\n", wordwrap($des, $x));
                                   $desc = $lines[0];
                                }
                        }
                            
                    ?>
                    <div class="post<?php echo $count ?>">
                            <a href="<?php the_permalink(); ?>" style="background-image:url(<?php echo the_post_thumbnail_url($post_id, array( 620, 300)); ?>); ">
                            <div class="contentpost">
                                <div class="post-featured-image" style="background-image:url(<?php echo the_post_thumbnail_url($post_id, array( 620, 300)); ?>); 
                                                                        background-position: center;
                                                                        background-size: cover;">
                                </div> 
                                <div class="post-featured-content">
                                    <div class="post-featured-title">
                                        <a href="<?php the_permalink(); ?>"><?php echo the_title(); ?>  <i class="angle right icon"></i></a>
                                    </div>
                                    <div class="post-featured-desc">
                                        <a href="<?php the_permalink(); ?>"><?php echo $desc; ?></a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach; 
                    wp_reset_postdata();
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
       <!--End featured-->
       <!--Title page-->
       <?php
            $post_id =  get_the_ID();
            $title = '';
            if ( is_home() ) {
                if ( get_option('page_for_posts') > 0 ) {
                    $post_id = get_option('page_for_posts');
                    $title = get_the_title( $post_id );
                }
            } else {
                $title = get_the_title(  );
            }
            
        ?>

        <section class="page-header container" style="">
                <div class="inner">
                    <div class="inner-content clearfix">
                        <div class="header-content">
                            <h1>
                               Our Blog
                            </h1>
                        </div>
                    </div>
                </div>
        </section>
       <!--End title page-->
       <div class="container">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
                    <?php
                    global $wp_query;
                    if ( ! is_singular() ) {
                        if ( have_posts() ) {

                            if ( is_search() && get_query_var('post_type') == 'store' ) {
                                get_template_part('search-store');
                            } else {
                                get_template_part( 'content','loop' );
                            }

                            get_template_part( 'content', 'paging' );

                        } else {
                            get_template_part('content','none');
                        }
                    } else {
                        get_template_part('content');
                    }
                    ?>

                </main><!-- #main -->
            </div><!-- #primary -->

            <?php

            if ( $layout != 'no-sidebar' ) {
                get_sidebar();
            }

            ?>
    </div>
    </div> <!-- /#content-wrap -->
<script>
    jQuery(function($) {
        $('input.search-field').attr("placeholder","Search Our Blog");
    });
</script>
<?php get_footer(); ?>
